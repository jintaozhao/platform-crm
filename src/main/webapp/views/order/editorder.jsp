<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>订单合同添加</title>
    <link rel="stylesheet" href="../../layui/css/layui.css">
    <script src="../../js/myutil.js"></script>
    <script src="../../layui/layui.js"></script>
</head>
<body>
<!-- 编辑添加框 -->
<form class="layui-form" lay-filter="order-form" style="width: 90%; padding: 20px;">

    <div class="layui-form-item layui-hide">
        <label class="layui-form-label">ID</label>
        <div class="layui-input-inline">
            <input type="text" name="id" value="0" readonly="readonly" lay-verify="required" placeholder=""
                   autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item" id="customer" style="display: none">
        <label class="layui-form-label">客户名称：</label>
        <div class="layui-input-inline" style="width: 320px;">
            <select class="layui-input" id="customerIdSelect" name="customerId" lay-verify="customerSelect">
                <option value="-1">--数据加载中--</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">签订时间：</label>
        <div class="layui-input-inline" style="width: 320px;">
            <input type="text" name="date" id="date" lay-verify="required" class="layui-input"/>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">签订地点：</label>
        <div class="layui-input-inline" style="width: 320px;">
            <input type="text" name="address" lay-verify="required" class="layui-input"/>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">合同编号：</label>
        <div class="layui-input-inline" style="width: 320px">
            <input type="text" name="orderNum" placeholder="请输入合同编号" class="layui-input"/>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">合同金额：</label>
        <div class="layui-input-inline" style="width: 320px">
            <input type="text" name="price" placeholder="价格单位：元(人民币)" lay-verify="required"
                   class="layui-input" onkeyup="checknum(this)" />
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">合同状态：</label>
        <div class="layui-input-inline" style="width: 320px">
            <select name="status" lay-filter="status">
                <option value="0">进行中</option>
                <option value="1">已完成</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 180px;">
            <button type="reset" class="layui-btn">重置</button>
            <button type="button" class="layui-btn" lay-submit lay-filter="type-submit">保存</button>
        </div>
    </div>
</form>


<script type="text/javascript">
    layui.use(['form', 'laydate'], function () {
        var form = layui.form;
        var layer = layui.layer;
        var laydate = layui.laydate;
        var $ = layui.$;

        var param = getParm();

        //渲染日期选择器
        laydate.render({
            elem: '#date',
            type: 'datetime'
        });

        // 截取url 参数值（这里默认一个参数id）
        var url = '${pageContext.request.contextPath}/orders/save';

        //根据id判断添加还是更新，客户都会显示，新建则不选择，更新时默认选择之前的客户
        showCustomer(true, param.id);

        function showCustomer(isShow, id) {
            if (isShow) {
                //显示客户选择框
                $('#customer').show();
                //加载客户列表
                $.post('${pageContext.request.contextPath}/customer/list', {'findtype': 'all'}, function (data) {
                    var customers = data.data;
                    var str = '<option value="">--请选择客户--</option>';
                    for (var i = 0; i < customers.length; i++) {
                        if (isShow && !id) {
                            str += '<option value="' + customers[i].id + '">' + customers[i].name + '</option>';
                        } else if (isShow && id) {
                            str += '<option selected="selected" value="' + customers[i].id + '">' + customers[i].name + '</option>';
                        }
                    }
                    $('select[name=customerId]').html(str);
                    form.render('select');
                });
            }
            if (isShow && id) {
                $.post('${pageContext.request.contextPath}/orders/get', {'id': id}, function (res) {
                    if (res.success) {
                        var data = res.data;
                        //设置值
                        form.val('order-form', {
                            "id": data.id,
                            "date": data.date,
                            "status": data.status,
                            "customerId": data.customerId,
                            "address": data.address,
                            "price": data.price,
                            "orderNum": data.orderNum
                        });
                    }
                });
            }
        }

        //表单自定义验证
        form.verify({
            customerSelect: function (value) {
                if (value == null || value == '' || value == '-1') {
                    return '请选择客户';
                }
            }
        });

        //监听表单提交按钮
        form.on('submit(type-submit)', function (data) {
            var btn = data.elem;         //获取点击的按钮
            btn.className += " layui-btn-disabled"; //给按钮添加禁用样式
            var formdata = data.field;//读取form表单中的数据 ,键值对形式

            console.log(formdata);

            //显示加载动画
            var index = top.layer.msg('数据提交中，请稍候', {
                icon: 16,
                time: false,
                shade: 0.8
            });

            $.ajax({
                type: "POST",
                url: url,
                data: formdata,
                dataType: "json",
                success: function (data) {
                    top.layer.msg(data.msg);	//使用top显示
                    if (data.success) {//成功
                        //关闭当前弹出层
                        var thisindex = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(thisindex);
                    }
                },
                error: function () {
                    top.layer.msg("服务器开小差了，请稍后再试...");
                },
                complete: function () {//请求完成执行，无论请求是否成功
                    btn.className = btn.className.replace("layui-btn-disabled", "");  //去除禁用属性
                    top.layer.close(index);		//关闭加载动画
                }
            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });

    });
</script>

</body>
</html>