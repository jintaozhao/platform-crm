package com.rocky.crm.controller;

import com.rocky.crm.pojo.*;
import com.rocky.crm.service.ICustomerService;
import com.rocky.crm.service.IOrderItemService;
import com.rocky.crm.service.IOrdersService;
import com.rocky.crm.utils.Operation;
import com.rocky.crm.utils.StringUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单控制器
 *
 * @author Rocky
 */
@Operation(name = "订单控制器")
@Controller
@RequestMapping("/orders")
public class OrdersController extends BaseController {

	@Autowired
	private IOrdersService ordersService;

	@Autowired
	private IOrderItemService orderItemService;

	@Autowired
	private ICustomerService customerService;

	/**
	 * 客户订单添加
	 *
	 * @author Rocky
	 * @date 2017/7/17
	 */
	@RequiresPermissions("crm:order:add")
	@Operation(name = "客户订单添加")
	@RequestMapping("/save")
	@ResponseBody
	public Map<String, Object> addOrder(Orders orders) {
		Map<String, Object> map = new HashMap<>();

		boolean success;
		if (orders != null && orders.getId() > 0) {
			success = ordersService.updateOrderByPrimaryKey(orders);
		} else {
			success = ordersService.insertOrder(orders);
		}
		String msg = success ? "保存成功" : "保存失败";

		map.put("success", success);
		map.put("msg", msg);

		return map;
	}

	/**
	 * 描述:客户订单子项添加
	 *
	 * @author Rocky
	 * @date 2017/7/17
	 */
	@RequiresPermissions("crm:order:add")
	@Operation(name = "客户订单子项添加")
	@RequestMapping("/addItem")
	@ResponseBody
	public Map<String, Object> addOrderItem(OrderItem item) {
		Map<String, Object> map = new HashMap<>();
		if (orderItemService.insertOrderItem(item)) {
			map.put("success", true);
			map.put("msg", "添加成功");
		} else {
			map.put("success", false);
			map.put("msg", "添加失败");
		}
		return map;
	}

	/**
	 * 描述:客户订单子项添加
	 *
	 * @author Rocky
	 * @date 2017/7/17
	 */
	@RequiresPermissions("crm:order:update")
	@Operation(name = "客户订单子项更新")
	@RequestMapping("/updateItem")
	@ResponseBody
	public Map<String, Object> editOrderItem(OrderItem item) {
		Map<String, Object> map = new HashMap<>();
		if (orderItemService.updateOrderItemByPrimaryKey(item)) {
			map.put("success", true);
			map.put("msg", "更新成功");
		} else {
			map.put("success", false);
			map.put("msg", "更新失败");
		}
		return map;
	}

	/**
	 * 描述:客户订单子项添加
	 *
	 * @author Rocky
	 * @date 2017/7/17
	 */
	@RequiresPermissions("crm:order:update")
	@Operation(name = "客户订单子项更新")
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> editOrder(Orders orders) {
		Map<String, Object> map = new HashMap<>();
		if (ordersService.updateOrderByPrimaryKey(orders)) {
			map.put("success", true);
			map.put("msg", "更新成功");
		} else {
			map.put("success", false);
			map.put("msg", "更新失败");
		}
		return map;
	}

	/**
	 * 描述:客户订单子项删除
	 *
	 * @author Rocky
	 * @date 2017/7/17
	 */
	@RequiresPermissions("crm:order:delete")
	@Operation(name = "客户订单子项删除")
	@RequestMapping("/deleteItem")
	@ResponseBody
	public Map<String, Object> deleteOrderItem(OrderItem item) {
		Map<String, Object> map = new HashMap<>();
		if (orderItemService.deleteByPrimaryKey(item.getId())) {
			map.put("success", true);
			map.put("msg", "删除订单子项成功");
		} else {
			map.put("success", false);
			map.put("msg", "删除订单子项失败");
		}
		return map;
	}


	/**
	 * 描述:客户订单删除
	 *
	 * @author Rocky
	 * @date 2017/7/17
	 */
	@RequiresPermissions("crm:order:delete")
	@Operation(name = "客户订单删除")
	@RequestMapping("/batchDelete")
	@ResponseBody
	public Map<String, Object> delete(int[] ids) {
		Map<String, Object> map = new HashMap<>();
		List<Integer> success = new ArrayList<Integer>();
		List<Integer> fail = new ArrayList<Integer>();
		Orders orders = null;

		if (ids == null) {
			map.put("msg", "参数为空，删除失败");
			map.put("status", false);
			return map;
		}
		for (int id : ids) {
			orders = ordersService.selectOrderByPrimaryKey(id);
			if (orders == null || orders.getStatus() == 0) {
				fail.add(id);
				continue;
			}
			if (ordersService.deleteByPrimaryKey(id)) {
				OrderItemExample orderItemExample = new OrderItemExample();
				OrderItemExample.Criteria criteria = orderItemExample.createCriteria();
				criteria.andOrdersIdEqualTo(id);
				// 根据order_id进行子项删除，返回结果不一定是true，因为不一定会有子项，所以此处不作为成功与否的判断
				orderItemService.deleteByOrderItemExample(orderItemExample);
				success.add(id);
			} else {
				fail.add(id);
			}
		}

		map.put("msg", "删除完成");
		map.put("status", true);
		map.put("success", success);
		map.put("fail", fail);

		return map;
	}


	/**
	 * 描述:客户订单删除
	 *
	 * @author Rocky
	 * @date 2017/7/17
	 */
	@RequiresPermissions("crm:order:delete")
	@Operation(name = "客户订单删除")
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> deleteOrder(int id) {
		Map<String, Object> map = new HashMap<>();

		boolean success = ordersService.deleteByPrimaryKey(id);

		if (success) {
			OrderItemExample orderItemExample = new OrderItemExample();
			OrderItemExample.Criteria criteria = orderItemExample.createCriteria();
			criteria.andOrdersIdEqualTo(id);
			// 根据order_id进行子项删除，返回结果不一定是true，因为不一定会有子项，所以此处不作为成功与否的判断
			orderItemService.deleteByOrderItemExample(orderItemExample);
		}

		if (success) {
			map.put("success", true);
			map.put("msg", "删除成功");
		} else {
			map.put("success", false);
			map.put("msg", "删除失败");
		}
		return map;
	}


	/**
	 * 描述:客户订单查询
	 *
	 * @author Rocky
	 * @date 2017/7/17
	 */
	@RequiresPermissions("crm:order:view")
	@Operation(name = "客户订单删除")
	@RequestMapping("/get")
	@ResponseBody
	public Map<String, Object> getOrder(int id) {
		Map<String, Object> map = new HashMap<>();

		Orders order = ordersService.selectOrderByPrimaryKey(id);

		if (order != null) {
			map.put("data", order);
			map.put("success", true);
			map.put("msg", "成功");
		} else {
			map.put("success", false);
			map.put("msg", "失败");
		}

		return map;
	}


	/**
	 * 获取客户的历史订单信息
	 *
	 * @param customer
	 * @return
	 * @author Rocky
	 */
	@Operation(name = "获取客户的历史订单")
	@RequiresAuthentication
	@RequiresPermissions("crm:order:view")
	@RequestMapping("/getHistoryOrdersByCustomer")
	@ResponseBody
	public Map<String, Object> getHistoryOrdersByCustomer(Long page, Long limit, Customer customer) {

		Map<String, Object> maps = new HashMap<>();

		Pager pager = new Pager(page.intValue(), limit.intValue());
		OrdersExample ordersExample = new OrdersExample();
		if (customer.getId() != null) {
			ordersExample.createCriteria().andCustomerIdEqualTo(customer.getId());
		}
		if (StringUtils.isNotEmpty(customer.getName())) {
			CustomerExample example = new CustomerExample();
			example.createCriteria().andNameLike(StringUtils.format("%{}%", customer.getName()));
			List<Customer> customers = customerService.selectByCustomerExample(example);
			List<Integer> customerIds = new ArrayList<>();
			for (Customer c : customers) {
				customerIds.add(c.getId());
			}
			ordersExample.createCriteria().andCustomerIdIn(customerIds);
		}
		ordersExample.setOrderByClause(" date desc");

		List<Orders> orders = ordersService.selectByOrdersExample(ordersExample, pager);
		for (Orders order : orders) {
			Customer linkCustomer = customerService.selectCustomerByPrimaryKey(order.getCustomerId());
			order.setCustomer(linkCustomer);
		}
		maps.put("data", orders);
		maps.put("count", pager.getTotal());
		maps.put("code", 0);
		return maps;
	}

	/**
	 * 获取订单的订单项
	 *
	 * @param orders
	 * @return
	 * @author Rocky
	 */
	@Operation(name = "获取订单的订单项")
	@RequiresAuthentication
	@RequestMapping("/getOrderItem")
	@ResponseBody
	public Map<String, Object> getOrderItem(Long page, Long limit, Orders orders) {

		Map<String, Object> maps = new HashMap<>();

		Pager pager = new Pager(page.intValue(), limit.intValue());

		OrderItemExample orderItemExample = new OrderItemExample();
		orderItemExample.createCriteria().andOrdersIdEqualTo(orders.getId());
		List<OrderItem> orderItems = orderItemService.selectByOrderItemExample(orderItemExample, pager);

		maps.put("code", 0);
		maps.put("data", orderItems);
		maps.put("count", pager.getTotal());
		return maps;
	}

}
