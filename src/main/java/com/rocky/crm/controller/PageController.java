package com.rocky.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @classname PageController
 * @description <br>页面</br>
 * @author  Rocky
 * @date    2019-12-09 14:15
 * @email   jintaozhao@qq.com
 * @version 1.0.0
 */
@Controller
@RequestMapping
public class PageController {

	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/unauthorized")
	public String unauthorized() {
		return "unauthorized";
	}
}
