package com.rocky.crm.controller;

import com.rocky.crm.pojo.Role;
import com.rocky.crm.pojo.SysDept;
import com.rocky.crm.pojo.TreeNode;
import com.rocky.crm.pojo.User;
import com.rocky.crm.service.ISysDeptService;
import com.rocky.crm.utils.Operation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.reflect.generics.tree.Tree;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Rocky
 * @version 1.0.0
 * @类名 SysDeptController
 * @描述 <br>部门信息</br>
 * @date 2019-04-23
 * @email jintaozhao@qq.com
 */
@Controller
@RequestMapping("/dept")
public class DeptController {

	@Autowired
	private ISysDeptService deptService;

	@RequiresPermissions("crm:dept:list")
	@RequestMapping("/list")
	@ResponseBody
	public List<SysDept> list(SysDept dept) {
		List<SysDept> deptList = deptService.selectDeptList(dept);
		return deptList;
	}

	@RequiresPermissions("crm:dept:list")
	@RequestMapping("/getDept")
	@ResponseBody
	public Map<String, Object> getDept(Long deptId) {
		SysDept dept = deptService.selectDeptById(deptId);
		Map<String, Object> map = new HashMap<>();
		map.put("success", true);
		map.put("code", 0);
		map.put("data", dept);
		return map;
	}

	/**
	 * 新增保存部门
	 */
	@Operation(name = "保存新增部门信息")
	@RequiresPermissions("crm:dept:add")
	@RequestMapping("/save")
	@ResponseBody
	public Map<String, Object> addSave(SysDept dept) {
		//从session域中获取当前登录的用户对象
		User user = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
		dept.setCreateUser(user.getAccount());
		dept.setCreateTime(new Date());

		Map<String, Object> map = new HashMap<>();
		List<SysDept> deptList = deptService.selectDeptList(dept);
		if (deptList.size() == 0) {
			if (deptService.insertDept(dept) > 0) {
				map.put("success", true);
				map.put("code", 0);
				map.put("msg", "添加成功");
			} else {
				map.put("success", false);
				map.put("code", -1);
				map.put("msg", "添加失败");
			}
		} else {
			map.put("success", false);
			map.put("code", -1);
			map.put("msg", "添加失败：部门已存在");
		}
		return map;
	}

	/**
	 * 保存
	 */
	@Operation(name = "部门管理")
	@RequiresPermissions("crm:dept:edit")
	@PostMapping("/update")
	@ResponseBody
	public Map<String, Object> editSave(SysDept dept) {
		//从session域中获取当前登录的用户对象
		User user = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
		dept.setUpdateUser(user.getAccount());
		dept.setUpdateTime(new Date());

		Map<String, Object> map = new HashMap<>();
		if (deptService.updateDept(dept) > 0) {
			map.put("success", true);
			map.put("code", 0);
			map.put("msg", "更新成功");
		} else {
			map.put("success", false);
			map.put("code", -1);
			map.put("msg", "更新失败");
		}

		return map;
	}

	/**
	 * 删除
	 */
	@Operation(name = "删除部门")
	@RequiresPermissions("crm:dept:remove")
	@PostMapping("/remove")
	@ResponseBody
	public Map remove(@RequestParam("deptId") Long deptId) {
		Map<String, Object> map = new HashMap<>();
		if (deptService.selectDeptCount(deptId) > 0) {
			map.put("success", false);
			map.put("code", -1);
			map.put("msg", "存在下级部门,不允许删除");
			return map;
		}
		if (deptService.checkDeptExistUser(deptId)) {
			map.put("success", false);
			map.put("code", -1);
			map.put("msg", "部门存在用户,不允许删除");
			return map;
		}
		if (deptService.deleteDeptById(deptId) > 0) {
			map.put("success", true);
			map.put("code", 0);
			map.put("msg", "删除成功");
		}
		return map;
	}

	/**
	 * 校验部门名称
	 */
	@PostMapping("/checkDeptNameUnique")
	@Operation(name = "校验部门")
	@ResponseBody
	public String checkDeptNameUnique(SysDept dept) {
		return deptService.checkDeptNameUnique(dept);
	}

	/**
	 * 加载部门列表树
	 */
	@PostMapping("/treeData")
	@Operation(name = "加载部门列表树信息")
	@ResponseBody
	public List<TreeNode> treeData() {
		List<TreeNode> tree = deptService.selectDeptTree(new SysDept());
		return tree;
	}

	/**
	 * 加载角色部门（数据权限）列表树
	 */
	@GetMapping("/roleDeptTreeData")
	@Operation(name = "加载角色部门（数据权限）列表树")
	@ResponseBody
	public List<Map<String, Object>> deptTreeData(Role role) {
		List<Map<String, Object>> tree = deptService.roleDeptTreeData(role);
		return tree;
	}
}
