package com.rocky.crm.controller;

import com.rocky.crm.utils.DateUtils;
import com.rocky.crm.utils.LocalDateTimeConverter;
import com.rocky.crm.utils.LocalDateTimeUtil;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @classname BaseController
 * @description <br>基础转换</br>
 * @author  Rocky
 * @date    2019-12-25 17:32
 * @email   jintaozhao@qq.com
 * @version 1.0.0
 */
public class BaseController {
	/**
	 * 将前台传递过来的日期格式的字符串，自动转化为Date类型
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		// Date 类型转换
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				setValue(DateUtils.parseDate(text));
			}
		});
	}
}
