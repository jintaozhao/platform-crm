package com.rocky.crm.controller;

import com.rocky.crm.pojo.Category;
import com.rocky.crm.pojo.CategoryExample;
import com.rocky.crm.pojo.Product;
import com.rocky.crm.pojo.ProductExample;
import com.rocky.crm.pojo.ProductExample.Criteria;
import com.rocky.crm.service.ICategoryService;
import com.rocky.crm.service.IProductService;
import com.rocky.crm.utils.Operation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Rocky
 */
@Operation(name = "产品管理")
@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private IProductService productService;

	@Autowired
	private ICategoryService categoryService;

	private Logger logger = LoggerFactory.getLogger(ProductController.class);

	/**
	 * 分页查找产品
	 *
	 * @return Map
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Operation(name = "查看所有产品")
	@RequestMapping("/findProduct")
	@ResponseBody
	public Map<String, Object> findProduct(Integer page, Integer limit, Product product) {

		Map<String, Object> map = new HashMap<>();

		ProductExample productExample = new ProductExample();
		Criteria criteria = productExample.createCriteria();
		productExample.setOrderByClause("id DESC");
		// 分页数据合法性判断
		if (page == null || page <= 0) {
			page = 1;
		}
		if (limit == null || limit <= 0) {
			limit = 10;
		}
		// 偏移值，即从第几条数据开始查
		Long offset = new Long((page - 1) * limit);

		// where条件
		if (product.getId() != null) {
			criteria.andIdEqualTo(product.getId());
		}
		if (product.getName() != null) {
			criteria.andNameLike("%" + product.getName() + "%");
		}
		if (product.getLocation() != null) {
			criteria.andLocationLike("%" + product.getLocation() + "%");
		}
		if (product.getCategoryId() != null) {
			criteria.andCategoryIdEqualTo(product.getCategoryId());
		}

		Long count = productService.countByProductExample(productExample);

		//每页多少条
		productExample.setLimit(limit);
		productExample.setOffset(offset);

		List<Product> list = productService.selectByProductExample(productExample);

		logger.info(list.toString());

		map.put("data", list);
		map.put("code", 0);
		map.put("msg", "success");
		map.put("count", count);
		return map;
	}


	@Operation(name = "查看产品的所有分类")
	@RequestMapping("/findCategoryList")
	@ResponseBody
	public Map<String, Object> findCategoryList(Integer page, Integer limit, Category category) {
		Map<String, Object> map = new HashMap<>();

		CategoryExample categoryExample = new CategoryExample();
		com.rocky.crm.pojo.CategoryExample.Criteria criteria = categoryExample.createCriteria();

		//第几条开始分页
		Long offset = new Long((page - 1) * limit);
		//每页多少条
		categoryExample.setLimit(limit);
		categoryExample.setOffset(offset);

		if (category.getId() != null) {
			criteria.andIdEqualTo(category.getId());
		}
		if (category.getName() != null) {
			criteria.andNameLike("%" + category.getName() + "%");
		}

		List<Category> list = categoryService.selectByCategoryExample(categoryExample);
		Long count = categoryService.countByCategoryExample(categoryExample);
		map.put("data", list);
		map.put("code", 0);
		map.put("msg", "success");
		map.put("count", count);
		return map;
	}

	@Operation(name = "查看产品的所有分类")
	@RequestMapping("/findCategory")
	@ResponseBody
	public Map<String, Object> findCategory(Category category) {
		Map<String, Object> map = new HashMap<>();

		CategoryExample categoryExample = new CategoryExample();
		com.rocky.crm.pojo.CategoryExample.Criteria criteria = categoryExample.createCriteria();

		if (category.getId() != null) {
			criteria.andIdEqualTo(category.getId());
		}
		if (category.getName() != null) {
			criteria.andNameLike("%" + category.getName() + "%");
		}

		List<Category> list = categoryService.selectByCategoryExample(categoryExample);
		map.put("categorys", list);
		return map;
	}

	/**
	 * 添加产品
	 *
	 * @param product
	 * @return
	 */
	@Operation(name = "添加产品")
	@RequiresPermissions("crm:product:add")
	@RequestMapping("/addProduct")
	@ResponseBody
	public Map<String, Object> addProduct(Product product) {
		Map<String, Object> map = new HashMap<>();
		// 用于判断返回给前台的结果
		boolean success = false;
		if (productService.insertProduct(product)) {
			success = true;
		}
		map.put("success", success);
		map.put("code", 0);
		return map;
	}

	/**
	 * 编辑产品
	 *
	 * @param product
	 * @return
	 */
	@Operation(name = "编辑产品")
	@RequiresPermissions("crm:product:edit")
	@RequestMapping("/editProduct")
	@ResponseBody
	public Map<String, Object> editProduct(Product product) {
		Map<String, Object> map = new HashMap<>();
		// 用于判断返回给前台的结果
		boolean success = false;
		if (productService.updateProductByPrimaryKey(product)) {
			success = true;
		}
		map.put("success", success);
		map.put("code", 0);
		return map;
	}

	/**
	 * 编辑产品类别
	 *
	 * @param category
	 * @return
	 */
	@Operation(name = "编辑类别")
	@RequiresPermissions("15005")
	@RequestMapping("/editCategory")
	@ResponseBody
	public Map<String, Object> editCategory(Category category) {
		Map<String, Object> map = new HashMap<>();
		// 用于判断返回给前台的结果
		boolean success = false;
		if (categoryService.updateCategoryByPrimaryKey(category)) {
			success = true;
		}
		map.put("success", success);
		map.put("code", 0);
		return map;
	}

	/**
	 * 描述：添加类别
	 *
	 * @param category
	 * @return Map
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	@Operation(name = "添加类别")
	@RequiresPermissions("15003")
	@RequestMapping("/addCategory")
	@ResponseBody
	public Map<String, Object> addCategory(Category category) {
		Map<String, Object> map = new HashMap<>();
		// 用于判断返回给前台的结果
		boolean success = false;
		if (categoryService.insertCategory(category)) {
			success = true;
		}
		map.put("success", success);
		map.put("code", 0);
		return map;
	}

	@Operation(name = "删除产品")
	@RequiresPermissions("crm:product:delete")
	@RequestMapping("/deleteProduct")
	@ResponseBody
	public Map<String, Object> deleteProduct(Integer id) {
		Map<String, Object> map = new HashMap<>();
		// 用于判断返回给前台的结果
		boolean success = false;
		// 删除结果判断
		if (productService.deleteByPrimaryKey(id)) {
			success = true;
		}
		map.put("success", success);
		map.put("code", 0);
		return map;
	}


	@Operation(name = "删除类别")
	@RequiresPermissions("15004")
	@RequestMapping("/deleteCategory")
	@ResponseBody
	public Map<String, Object> deleteCategory(Integer id) {
		Map<String, Object> map = new HashMap<>();
		// 用于判断返回给前台的结果
		boolean success = false;
		// 删除结果判断
		if (categoryService.deleteByPrimaryKey(id)) {
			success = true;
		}
		map.put("success", success);
		map.put("code", 0);
		return map;
	}
}
