package com.rocky.crm.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rocky.crm.utils.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author Rocky
 * @version 1.0.0
 * @classname BaseEntity
 * @description Entity 基类
 * @date 2019-01-16 10:54
 * @email jintaozhao@qq.com
 */
public class BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 搜索值
	 */
	private String searchValue;

	/**
	 * 创建者
	 */
	private String createUser = "admin";

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime = new Date();

	/**
	 * 更新者
	 */
	private String updateUser = "admin";

	/**
	 * 更新时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	// @Excel(name = "更新时间", dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime = new Date();

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 请求参数
	 */
	private Map<String, Object> params;

	/**
	 * 分页总数
	 */
	private Long count;

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getCreateUser() {
		return StringUtils.isEmpty(createUser) ? "admin" : createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime == null ? new Date() : createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return StringUtils.isEmpty(updateUser) ? "admin" : updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateTime() {
		return updateTime == null ? new Date() : updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
}
