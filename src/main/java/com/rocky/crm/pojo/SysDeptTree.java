package com.rocky.crm.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @classname SysDeptTree
 * @description <br>部门树</br>
 * @author  Rocky
 * @date    2019-10-11 22:56
 * @email   jintaozhao@qq.com
 * @version 1.0.0
 */
@Data
public class SysDeptTree implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	private Long deptId;

	/**
	 * 父部门ID
	 */
	private Long parentId;

	/**
	 * 部门名称
	 */
	private String deptName;

	/**
	 * 部门主管
	 */
	private String leader;
	/**
	 * 父部门名称
	 */
	private String parentName;
	/**
	 * 显示顺序
	 */
	private String orderNum;

	/**
	 * 部门状态:0正常,1停用
	 */
	private String status;

	/**
	 * 子部门
	 */
	private List<SysDeptTree> children;

}
