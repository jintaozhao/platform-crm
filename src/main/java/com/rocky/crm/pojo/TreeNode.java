package com.rocky.crm.pojo;

import java.util.List;

/**
 * @author Rocky
 * @version 1.0.0
 * @classname TreeNode
 * @description <br>部门树对象</br>
 * @date 2019-12-18 16:52
 * @email jintaozhao@qq.com
 */
public class TreeNode {

	/**
	 * 数据id
	 */
	private Long id;
	/**
	 * 父ID
	 */
	private Long parentId;
	/**
	 * 节点名称
	 */
	private String name;
	/**
	 * 是否展开
	 */
	private boolean spread;
	/**
	 * 节点链接
	 */
	private String href;
	/**
	 * 子节点
	 */
	private List<TreeNode> children;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSpread() {
		return spread;
	}

	public void setSpread(boolean spread) {
		this.spread = spread;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
}
