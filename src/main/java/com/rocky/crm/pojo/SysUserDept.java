package com.rocky.crm.pojo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * @author Rocky
 * @version 1.0.0
 * @classname SysUserDept
 * @description <br>用户与部门关联表对应实体类</br>
 * @date 2019-09-27 16:05:08
 * @email jintaozhao@qq.com
 */
public class SysUserDept {
	private static final long serialVersionUID = 1L;


	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 部门ID
	 */
	private Long deptId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getDeptId() {
		return deptId;
	}

	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}

	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
				.append("userId", getUserId())
				.append("deptId", getDeptId())
				.toString();
	}
}
