/**
 *
 */
package com.rocky.crm.utils;

import org.apache.shiro.crypto.hash.SimpleHash;

/**
 * @author Rocky
 *
 */
public class Md5Util {

	/**
	 * 加密算法
	 */
	private static String algorithmName = "md5";
	/**
	 * 加密算法迭代加密次数
	 */
	private static int hashIterations = 2;

	/**
	 *
	 * 描述：对信息进行MD5盐值加密
	 * @author Rocky
	 * @version 1.0
	 * @param message
	 * @param salt
	 * @return String
	 * @exception
	 * @since 1.8
	 *
	 */
	public static String encrypt(String message, String salt) {
		SimpleHash hash = new SimpleHash(algorithmName, message, salt, hashIterations);
		return hash.toHex();
	}

	public static void main(String[] args) {
		// String password = Md5Util.encrypt("123456", "admin111111");
		// System.out.println(password);

		String password = Md5Util.encrypt("123456", "zxt222222");
		System.out.println(password);
	}


}
