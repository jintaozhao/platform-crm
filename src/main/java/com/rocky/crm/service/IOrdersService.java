package com.rocky.crm.service;

import com.rocky.crm.pojo.Orders;
import com.rocky.crm.pojo.OrdersExample;
import com.rocky.crm.pojo.Pager;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IOrdersService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param ordersExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByOrdersExample(OrdersExample ordersExample);

	/**
	 * 描述：按照Example 删除Order
	 *
	 * @param ordersExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByOrdersExample(OrdersExample ordersExample);

	/**
	 * 描述：按照Order主键id删除Order
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条Order数据 如字段为空，则插入null
	 *
	 * @param orders 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertOrder(Orders orders);

	/**
	 * 描述：插入一条Order数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param orders 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(Orders orders);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param ordersExample 查询条件
	 * @return List<Order> 含Order的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<Orders> selectByOrdersExample(OrdersExample ordersExample);

	/**
	 * 描述：按照Order 的id 查找 Order
	 *
	 * @param id 要查询的id
	 * @return Order 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	Orders selectOrderByPrimaryKey(Integer id);

	/**
	 * 描述：更新Order
	 *
	 * @param orders        对象中若有空则更新字段为null
	 * @param ordersExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByOrdersExample(Orders orders, OrdersExample ordersExample);

	/**
	 * 描述：更新Order
	 *
	 * @param orders        对象中若有空则不会更新此字段
	 * @param ordersExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByOrdersExampleSelective(Orders orders, OrdersExample ordersExample);

	/**
	 * 描述：按照Order id 更新Order
	 *
	 * @param orders 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateOrderByPrimaryKeySelective(Orders orders);

	/**
	 * 描述：按照Order id 更新Order
	 *
	 * @param orders 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateOrderByPrimaryKey(Orders orders);

	/**
	 * 描述：按照客户id分组查询最后一个订单的时间
	 *
	 * @return List<Orders>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Orders> selectOrdersGroupByCustomerId();

	/**
	 * 描述：分页查询客户历史订单信息
	 *
	 * @param ordersExample
	 * @param pager
	 * @return List<Orders>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Orders> selectByOrdersExample(OrdersExample ordersExample, Pager pager);
}
