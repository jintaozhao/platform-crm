package com.rocky.crm.service;

import com.rocky.crm.pojo.Pager;
import com.rocky.crm.pojo.Permission;
import com.rocky.crm.pojo.PermissionExample;

import java.util.List;

/**
 * @author Rocky
 */
public interface IPermissionService {

	/**
	 * 根据example统计permission条数
	 *
	 * @param permissionExample+
	 * @return long
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	long countByPermissionExample(PermissionExample permissionExample);

	/**
	 * 根据example删除permission
	 *
	 * @param permissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deleteByPermissionExample(PermissionExample permissionExample);

	/**
	 * 根据主键删除permission
	 *
	 * @param id
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deletePermissionByPrimaryKey(Integer id);

	/**
	 * 插入一条permission，如果字段为空，那么插入null
	 *
	 * @param permission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean insertPermission(Permission permission);

	/**
	 * 插入一条permission，如果字段为空，插入的是数据库中字段的默认值
	 *
	 * @param permission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean insertPermissionSelective(Permission permission);

	/**
	 * 根据example查找permission 分页
	 *
	 * @param permissionExample
	 * @param pager
	 * @return List<Permission>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	List<Permission> selectByPermissionExample(PermissionExample permissionExample, Pager pager);

	/**
	 * 根据example查找permission
	 *
	 * @param permissionExample
	 * @param pager
	 * @return List<Permission>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	List<Permission> selectByPermissionExample(PermissionExample permissionExample);


	/**
	 * 根据主键id查找permission
	 *
	 * @param id
	 * @return Permission
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	Permission selectPermissionByPrimaryKey(Integer id);

	/**
	 * 更新permission，参数permission中如果某个字段为空代表不更新此字段，example为where条件
	 *
	 * @param permission
	 * @param permissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateByPermissionExampleSelective(Permission permission, PermissionExample permissionExample);

	/**
	 * 更新permission，参数permission中如果某个字段为空代表此字段也更新为空，example为where条件
	 *
	 * @param permission
	 * @param permissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateByPermissionExample(Permission permission, PermissionExample permissionExample);

	/**
	 * 根据主键id更新permission，参数permission中某些字段为空，代表不更新此字段
	 *
	 * @param permission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updatePermissionByPrimaryKeySelective(Permission permission);

	/**
	 * 根据主键id更新permission，参数permission中如果有字段为空，代表更新为null
	 *
	 * @param permission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updatePermissionByPrimaryKey(Permission permission);

	/**
	 * 查询权限树
	 *
	 * @param
	 * @return List<Permission>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	List<Permission> selectTreePermission();

	/**
	 * 描述：根据当前权限的id查询该权限下的所有子权限
	 *
	 * @param id
	 * @return List<Permission>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Permission> selectChildPermission(Integer id);

	/**
	 * 描述：设置当前权限为顶级权限
	 *
	 * @param permission
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updatePermissionSetTopPermission(Permission permission);
}
