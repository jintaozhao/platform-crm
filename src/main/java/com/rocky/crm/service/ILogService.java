package com.rocky.crm.service;

import com.rocky.crm.pojo.Log;
import com.rocky.crm.pojo.LogExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ILogService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param logExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByLogExample(LogExample logExample);

	/**
	 * 描述：按照Example 删除Log
	 *
	 * @param logExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByLogExample(LogExample logExample);

	/**
	 * 描述：按照Log主键id删除Log
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条Log数据 如字段为空，则插入null
	 *
	 * @param log 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertLog(Log log);

	/**
	 * 描述：插入一条Log数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param log 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(Log log);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param logExample 查询条件
	 * @return List<Log> 含Log的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<Log> selectByLogExample(LogExample logExample);

	/**
	 * 描述：按照Log 的id 查找 Log
	 *
	 * @param id 要查询的id
	 * @return Log 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	Log selectLogByPrimaryKey(Integer id);

	/**
	 * 描述：更新Log
	 *
	 * @param log        对象中若有空则更新字段为null
	 * @param logExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByLogExample(Log log, LogExample logExample);

	/**
	 * 描述：更新Log
	 *
	 * @param log        对象中若有空则不会更新此字段
	 * @param logExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByLogExampleSelective(Log log, LogExample logExample);

	/**
	 * 描述：按照Log id 更新Log
	 *
	 * @param log 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateLogByPrimaryKeySelective(Log log);

	/**
	 * 描述：按照Log id 更新Log
	 *
	 * @param log 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateLogByPrimaryKey(Log log);
}
