package com.rocky.crm.service;

import com.rocky.crm.pojo.CustomerLoss;
import com.rocky.crm.pojo.CustomerLossExample;
import com.rocky.crm.pojo.Pager;
import com.rocky.crm.pojo.User;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ICustomerLossService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param customerLossExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByCustomerLossExample(CustomerLossExample customerLossExample);

	/**
	 * 描述：按照Example 删除CustomerLoss
	 *
	 * @param customerLossExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByCustomerLossExample(CustomerLossExample customerLossExample);

	/**
	 * 描述：按照CustomerLoss主键id删除CustomerLoss
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条CustomerLoss数据 如字段为空，则插入null
	 *
	 * @param customerLoss 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertCustomerLoss(CustomerLoss customerLoss);

	/**
	 * 描述：插入一条CustomerLoss数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param customerLoss 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(CustomerLoss customerLoss);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param customerLossExample 查询条件
	 * @param pager
	 * @return List<CustomerLoss> 含CustomerLoss的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<CustomerLoss> selectByCustomerLossExample(CustomerLossExample customerLossExample);

	/**
	 * 描述：按照CustomerLoss 的id 查找 CustomerLoss
	 *
	 * @param id 要查询的id
	 * @return CustomerLoss 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	CustomerLoss selectCustomerLossByPrimaryKey(Integer id);

	/**
	 * 描述：更新CustomerLoss
	 *
	 * @param customerLoss        对象中若有空则更新字段为null
	 * @param customerLossExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCustomerLossExample(CustomerLoss customerLoss, CustomerLossExample customerLossExample);

	/**
	 * 描述：更新CustomerLoss
	 *
	 * @param customerLoss        对象中若有空则不会更新此字段
	 * @param customerLossExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCustomerLossExampleSelective(CustomerLoss customerLoss, CustomerLossExample customerLossExample);

	/**
	 * 描述：按照CustomerLoss id 更新CustomerLoss
	 *
	 * @param customerLoss 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCustomerLossByPrimaryKeySelective(CustomerLoss customerLoss);

	/**
	 * 描述：按照CustomerLoss id 更新CustomerLoss
	 *
	 * @param customerLoss 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCustomerLossByPrimaryKey(CustomerLoss customerLoss);

	/**
	 * 描述：
	 *
	 * @param customerLoss
	 * @param pager
	 * @param user
	 * @return List<CustomerLoss>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<CustomerLoss> selectByCustomerLossExampleByPager(CustomerLoss customerLoss, Pager pager, User user);
}