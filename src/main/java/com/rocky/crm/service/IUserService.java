package com.rocky.crm.service;

import com.rocky.crm.pojo.User;
import com.rocky.crm.pojo.UserExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IUserService {

	/**
	 * 描述：根据用户名查找用户
	 *
	 * @param account
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	User findByAccount(String account);

	/**
	 * 描述：添加用户
	 *
	 * @param user
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean save(User user);

	/**
	 * 描述：根据id删除用户
	 *
	 * @param id
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteById(Integer id);

	/**
	 * 描述：根据id查找用户
	 *
	 * @param id
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	User findById(Integer id);

	/**
	 * 描述：根据模板查找用户
	 *
	 * @param userExample 用户查询模板
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<User> findByExample(UserExample userExample);

	/**
	 * 描述：根据模板统计数量
	 *
	 * @param userExample
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByExample(UserExample userExample);

	/**
	 * 描述：根据id修改用户信息
	 *
	 * @param user
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean edit(User user);

	/**
	 * 描述:对用户的密码进行加密
	 *
	 * @param user
	 * @return User
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	User encryptPassword(User user);

	/**
	 * 描述：修改用户密码
	 *
	 * @param user
	 * @param oldPassword
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean editPasswd(User user, String oldPassword);

	/**
	 * 描述：根据邮箱查找用户
	 *
	 * @param email
	 * @return User
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	User findByEmail(String email);

}
