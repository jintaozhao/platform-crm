package com.rocky.crm.service;


import com.rocky.crm.pojo.User;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ILoginService {

	/**
	 * 描述：登录
	 *
	 * @param user
	 * @return User
	 * @author
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	User login(User user);
}
