package com.rocky.crm.service;

import com.rocky.crm.pojo.RolePermission;
import com.rocky.crm.pojo.RolePermissionExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IRolePermissionService {

	/**
	 * 根据RolePermissionExample统计条数
	 *
	 * @param rolePermissionExample
	 * @return long
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	long countByRolePermissionExample(RolePermissionExample rolePermissionExample);

	/**
	 * 根据RolePermissionExample删除rolePermission
	 *
	 * @param rolePermissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deleteByRolePermissionExample(RolePermissionExample rolePermissionExample);

	/**
	 * 根据主键id删除rolePermission
	 *
	 * @param id
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deleteRolePermissionByPrimaryKey(Integer id);

	/**
	 * 插入一条数据，如果字段为空，插入的字段也为空
	 *
	 * @param rolePermission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean insertRolePermission(RolePermission rolePermission);

	/**
	 * 插入一条数据，如果参数rolePermission中字段为空，代表字段为数据库默认值
	 *
	 * @param rolePermission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean insertRolePermissionSelective(RolePermission rolePermission);

	/**
	 * 根据RolePermissionExample查找
	 *
	 * @param rolePermissionExample
	 * @return List<RolePermission>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	List<RolePermission> selectByRolePermissionExample(RolePermissionExample rolePermissionExample);

	/**
	 * 根据主键id查找RolePermission
	 *
	 * @param id
	 * @return RolePermission
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	RolePermission selectRolePermissionByPrimaryKey(Integer id);

	/**
	 * 更新RolePermission，如果传入的参数rolePermission中字段为空，代表不更新此字段，RolePermissionExample为where条件
	 *
	 * @param rolePermission
	 * @param rolePermissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateByRolePermissionExampleSelective(RolePermission rolePermission, RolePermissionExample rolePermissionExample);

	/**
	 * 更新RolePermission，如果传入的参数rolePermission中字段为空，代表此字段也更新为空，RolePermissionExample为where条件
	 *
	 * @param rolePermission
	 * @param rolePermissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateByRolePermissionExample(RolePermission rolePermission, RolePermissionExample rolePermissionExample);

	/**
	 * 更新RolePermission，如果传入的参数rolePermission中字段为空，代表不更新此字段
	 *
	 * @param rolePermission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateRolePermissionByPrimaryKeySelective(RolePermission rolePermission);

	/**
	 * 更新RolePermission,如果传入的参数rolePermission中字段为空，那么相应字段也更新为空
	 *
	 * @param rolePermission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateRolePermissionByPrimaryKey(RolePermission rolePermission);


	/**
	 * 描述：分配权限给角色
	 *
	 * @param permissionIds
	 * @param roleId
	 * @return int
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int allotPermission(Integer[] permissionIds, Integer roleId);

}
