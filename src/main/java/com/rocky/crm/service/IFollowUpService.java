package com.rocky.crm.service;

import com.rocky.crm.pojo.FollowUp;
import com.rocky.crm.pojo.FollowUpExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IFollowUpService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param followUpExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByFollowUpExample(FollowUpExample followUpExample);

	/**
	 * 描述：按照Example 删除FollowUp
	 *
	 * @param followUpExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByFollowUpExample(FollowUpExample followUpExample);

	/**
	 * 描述：按照FollowUp主键id删除FollowUp
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条FollowUp数据 如字段为空，则插入null
	 *
	 * @param followUp 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertFollowUp(FollowUp followUp);

	/**
	 * 描述：插入一条FollowUp数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param followUp 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(FollowUp followUp);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param followUpExample 查询条件
	 * @return List<FollowUp> 含FollowUp的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<FollowUp> selectByFollowUpExample(FollowUpExample followUpExample);

	/**
	 * 描述：按照FollowUp 的id 查找 FollowUp
	 *
	 * @param id 要查询的id
	 * @return FollowUp 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	FollowUp selectFollowUpByPrimaryKey(Integer id);

	/**
	 * 描述：更新FollowUp
	 *
	 * @param followUp        对象中若有空则更新字段为null
	 * @param followUpExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByFollowUpExample(FollowUp followUp, FollowUpExample followUpExample);

	/**
	 * 描述：更新FollowUp
	 *
	 * @param followUp        对象中若有空则不会更新此字段
	 * @param followUpExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByFollowUpExampleSelective(FollowUp followUp, FollowUpExample followUpExample);

	/**
	 * 描述：按照FollowUp id 更新FollowUp
	 *
	 * @param followUp 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateFollowUpByPrimaryKeySelective(FollowUp followUp);

	/**
	 * 描述：按照FollowUp id 更新FollowUp
	 *
	 * @param followUp 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateFollowUpByPrimaryKey(FollowUp followUp);
}