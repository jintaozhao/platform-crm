package com.rocky.crm.service;

import com.rocky.crm.pojo.Service;
import com.rocky.crm.pojo.ServiceExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IServiceService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param serviceExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByServiceExample(ServiceExample serviceExample);

	/**
	 * 描述：按照Example 删除Service
	 *
	 * @param serviceExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByServiceExample(ServiceExample serviceExample);

	/**
	 * 描述：按照Service主键id删除Service
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条Service数据 如字段为空，则插入null
	 *
	 * @param service 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertService(Service service);

	/**
	 * 描述：插入一条Service数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param service 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(Service service);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param serviceExample 查询条件
	 * @return List<Service> 含Service的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<Service> selectByServiceExample(ServiceExample serviceExample);

	/**
	 * 描述：按照Service 的id 查找 Service
	 *
	 * @param id 要查询的id
	 * @return Service 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	Service selectServiceByPrimaryKey(Integer id);

	/**
	 * 描述：更新Service
	 *
	 * @param service        对象中若有空则更新字段为null
	 * @param serviceExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByServiceExample(Service service, ServiceExample serviceExample);

	/**
	 * 描述：更新Service
	 *
	 * @param service        对象中若有空则不会更新此字段
	 * @param serviceExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByServiceExampleSelective(Service service, ServiceExample serviceExample);

	/**
	 * 描述：按照Service id 更新Service
	 *
	 * @param service 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateServiceByPrimaryKeySelective(Service service);

	/**
	 * 描述：按照Service id 更新Service
	 *
	 * @param service 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateServiceByPrimaryKey(Service service);

	/**
	 * 描述：批量删除服务
	 *
	 * @param ids
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteServicesByPrimaryKey(String ids);
}
