package com.rocky.crm.service;

import com.rocky.crm.pojo.Pager;
import com.rocky.crm.pojo.SaleOpportunity;
import com.rocky.crm.pojo.SaleOpportunityExample;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ISaleOpportunityService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param saleOpportunityExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countBySaleOpportunityExample(SaleOpportunityExample saleOpportunityExample);

	/**
	 * 描述：按照Example 删除SaleOpportunity
	 *
	 * @param saleOpportunityExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteBySaleOpportunityExample(SaleOpportunityExample saleOpportunityExample);

	/**
	 * 描述：按照SaleOpportunity主键id删除SaleOpportunity
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条SaleOpportunity数据 如字段为空，则插入null
	 *
	 * @param saleOpportunity 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSaleOpportunity(SaleOpportunity saleOpportunity);

	/**
	 * 描述：插入一条SaleOpportunity数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param saleOpportunity 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(SaleOpportunity saleOpportunity);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param saleOpportunityExample 查询条件
	 * @return List<SaleOpportunity> 含SaleOpportunity的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<SaleOpportunity> selectBySaleOpportunityExample(SaleOpportunityExample saleOpportunityExample);

	/**
	 * 描述：根据传入的参数封装到example中，再进行selectBySaleOpportunityExample查询
	 *
	 * @param saleOpportunity
	 * @param pager
	 * @param request
	 * @return List<SaleOpportunity>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<SaleOpportunity> selectBySaleOpportunitySelectiveAndPager(SaleOpportunity saleOpportunity, Pager pager, HttpServletRequest request);


	/**
	 * 描述：按照SaleOpportunity 的id 查找 SaleOpportunity
	 *
	 * @param id 要查询的id
	 * @return SaleOpportunity 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	SaleOpportunity selectSaleOpportunityByPrimaryKey(Integer id);

	/**
	 * 描述：更新SaleOpportunity
	 *
	 * @param saleOpportunity        对象中若有空则更新字段为null
	 * @param saleOpportunityExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateBySaleOpportunityExample(SaleOpportunity saleOpportunity, SaleOpportunityExample saleOpportunityExample);

	/**
	 * 描述：更新SaleOpportunity
	 *
	 * @param saleOpportunity        对象中若有空则不会更新此字段
	 * @param saleOpportunityExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateBySaleOpportunityExampleSelective(SaleOpportunity saleOpportunity, SaleOpportunityExample saleOpportunityExample);

	/**
	 * 描述：按照SaleOpportunity id 更新SaleOpportunity
	 *
	 * @param saleOpportunity 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateSaleOpportunityByPrimaryKeySelective(SaleOpportunity saleOpportunity);

	/**
	 * 描述：按照SaleOpportunity id 更新SaleOpportunity
	 *
	 * @param saleOpportunity 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateSaleOpportunityByPrimaryKey(SaleOpportunity saleOpportunity);

	/**
	 * 根据id（分隔符是-）批量软删除销售机会
	 *
	 * @param ids
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deleteSaleOpportunitiesByPrimaryKey(String ids);

}

