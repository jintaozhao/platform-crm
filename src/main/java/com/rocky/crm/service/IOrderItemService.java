package com.rocky.crm.service;

import com.rocky.crm.pojo.OrderItem;
import com.rocky.crm.pojo.OrderItemExample;
import com.rocky.crm.pojo.Pager;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IOrderItemService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param orderItemExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByOrderItemExample(OrderItemExample orderItemExample);

	/**
	 * 描述：按照Example 删除OrderItem
	 *
	 * @param orderItemExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByOrderItemExample(OrderItemExample orderItemExample);

	/**
	 * 描述：按照OrderItem主键id删除OrderItem
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条OrderItem数据 如字段为空，则插入null
	 *
	 * @param orderItem 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertOrderItem(OrderItem orderItem);

	/**
	 * 描述：插入一条OrderItem数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param orderItem 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(OrderItem orderItem);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param orderItemExample 查询条件
	 * @return List<OrderItem> 含OrderItem的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<OrderItem> selectByOrderItemExample(OrderItemExample orderItemExample);

	/**
	 * 描述：按照OrderItem 的id 查找 OrderItem
	 *
	 * @param id 要查询的id
	 * @return OrderItem 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	OrderItem selectOrderItemByPrimaryKey(Integer id);

	/**
	 * 描述：更新OrderItem
	 *
	 * @param orderItem        对象中若有空则更新字段为null
	 * @param orderItemExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByOrderItemExample(OrderItem orderItem, OrderItemExample orderItemExample);

	/**
	 * 描述：更新OrderItem
	 *
	 * @param orderItem        对象中若有空则不会更新此字段
	 * @param orderItemExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByOrderItemExampleSelective(OrderItem orderItem, OrderItemExample orderItemExample);

	/**
	 * 描述：按照OrderItem id 更新OrderItem
	 *
	 * @param orderItem 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateOrderItemByPrimaryKeySelective(OrderItem orderItem);

	/**
	 * 描述：按照OrderItem id 更新OrderItem
	 *
	 * @param orderItem 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateOrderItemByPrimaryKey(OrderItem orderItem);

	/**
	 * 描述：分页获取订单的订单项
	 *
	 * @param orderItemExample
	 * @param pager
	 * @return List<OrderItem>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<OrderItem> selectByOrderItemExample(OrderItemExample orderItemExample, Pager pager);
}
