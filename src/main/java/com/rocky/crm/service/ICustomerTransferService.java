package com.rocky.crm.service;

import com.rocky.crm.pojo.CustomerTransfer;
import com.rocky.crm.pojo.CustomerTransferExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ICustomerTransferService {

	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param customerTransferExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByCustomerTransferExample(CustomerTransferExample customerTransferExample);

	/**
	 * 描述：按照Example 删除CustomerTransfer
	 *
	 * @param customerTransferExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByCustomerTransferExample(CustomerTransferExample customerTransferExample);

	/**
	 * 描述：按照CustomerTransfer主键id删除CustomerTransfer
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条CustomerTransfer数据 如字段为空，则插入null
	 *
	 * @param customerTransfer 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertCustomerTransfer(CustomerTransfer customerTransfer);

	/**
	 * 描述：插入一条CustomerTransfer数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param customerTransfer 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(CustomerTransfer customerTransfer);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param customerTransferExample 查询条件
	 * @return List<CustomerTransfer> 含CustomerTransfer的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<CustomerTransfer> selectByCustomerTransferExample(CustomerTransferExample customerTransferExample);

	/**
	 * 描述：按照CustomerTransfer 的id 查找 CustomerTransfer
	 *
	 * @param id 要查询的id
	 * @return CustomerTransfer 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	CustomerTransfer selectCustomerTransferByPrimaryKey(Integer id);

	/**
	 * 描述：更新CustomerTransfer
	 *
	 * @param customerTransfer        对象中若有空则更新字段为null
	 * @param customerTransferExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCustomerTransferExample(CustomerTransfer customerTransfer, CustomerTransferExample customerTransferExample);

	/**
	 * 描述：更新CustomerTransfer
	 *
	 * @param customerTransfer        对象中若有空则不会更新此字段
	 * @param customerTransferExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCustomerTransferExampleSelective(CustomerTransfer customerTransfer, CustomerTransferExample customerTransferExample);

	/**
	 * 描述：按照CustomerTransfer id 更新CustomerTransfer
	 *
	 * @param customerTransfer 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCustomerTransferByPrimaryKeySelective(CustomerTransfer customerTransfer);

	/**
	 * 描述：按照CustomerTransfer id 更新CustomerTransfer
	 *
	 * @param customerTransfer 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCustomerTransferByPrimaryKey(CustomerTransfer customerTransfer);
}