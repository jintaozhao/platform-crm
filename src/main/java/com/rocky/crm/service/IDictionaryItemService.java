package com.rocky.crm.service;

import com.rocky.crm.pojo.DictionaryItem;
import com.rocky.crm.pojo.DictionaryItemExample;

import java.util.List;

/**
 * @author Rocky
 */
public interface IDictionaryItemService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param dictionaryItemExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.1
	 * @since 1.8
	 */
	long countByDictionaryItemExample(DictionaryItemExample dictionaryItemExample);

	/**
	 * 描述：按照Example 删除 DictionaryItemExample
	 *
	 * @param dictionaryItemExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByDictionaryItemExample(DictionaryItemExample dictionaryItemExample);

	/**
	 * 描述：按照主键id删除DictionaryItem
	 *
	 * @param id 数据字典 id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条DictionaryItem数据 如字段为空，则插入null
	 *
	 * @param dictionaryItem
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertDictionaryItem(DictionaryItem dictionaryItem);

	/**
	 * 描述：插入一条DictionaryItem数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param dictionaryItem 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertDictionaryItemSelective(DictionaryItem dictionaryItem);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param dictionaryItemExample 查询条件
	 * @return List<DictionaryItem> 含DictionaryItem的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<DictionaryItem> selectByDictionaryItemExample(DictionaryItemExample dictionaryItemExample);

	/**
	 * 描述：按照DictionaryItem 的id 查找 DictionaryItem
	 *
	 * @param id 要查询的id
	 * @return DictionaryItem 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	DictionaryItem selectDictionaryItemByPrimaryKey(Integer id);

	/**
	 * 描述：更新DictionaryItem
	 *
	 * @param dictionaryItem        对象中若有空则更新字段为null
	 * @param dictionaryItemExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByDictionaryItemExample(DictionaryItem dictionaryItem, DictionaryItemExample dictionaryItemExample);

	/**
	 * 描述：更新DictionaryItem
	 *
	 * @param dictionaryItem        对象中若有空则不会更新此字段
	 * @param dictionaryItemExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByDictionaryItemExampleSelective(DictionaryItem dictionaryItem, DictionaryItemExample dictionaryItemExample);

	/**
	 * 描述：按照DictionaryItem id 更新DictionaryItem
	 *
	 * @param dictionaryItem 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateDictionaryItemByPrimaryKeySelective(DictionaryItem dictionaryItem);

	/**
	 * 描述：按照DictionaryItem id 更新DictionaryItem
	 *
	 * @param dictionaryItem 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateDictionaryItemByPrimaryKey(DictionaryItem dictionaryItem);
}
