package com.rocky.crm.service;

import com.rocky.crm.pojo.Linkman;
import com.rocky.crm.pojo.LinkmanExample;
import com.rocky.crm.pojo.User;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ILinkmanService {

	/**
	 * 根据linkmanExample统计条数
	 *
	 * @param linkmanExample
	 * @return long
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	long countByLinkmanExample(LinkmanExample linkmanExample);

	/**
	 * 根据linkmanExample删除Linkman
	 *
	 * @param linkmanExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deleteByLinkmanExample(LinkmanExample linkmanExample);

	/**
	 * 根据主键id删除Linkman
	 *
	 * @param id
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deleteLinkmanByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条Linkman数据，如果参数linkman中字段为空，代表此字段插入为空
	 *
	 * @param linkman
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertLinkman(Linkman linkman);

	/**
	 * 插入一条Linkman数据，如果参数linkman中字段为空，代表此字段为数据库默认值
	 *
	 * @param linkman
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean insertLinkmanSelective(Linkman linkman);

	/**
	 * 描述：根据linkmanExample查找Linkman
	 *
	 * @param linkmanExample
	 * @return List<Linkman>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Linkman> selectByLinkmanExample(LinkmanExample linkmanExample);

	/**
	 * 描述：
	 *
	 * @param id
	 * @return Linkman
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	Linkman selectLinkmanByPrimaryKey(Integer id);

	/**
	 * 更新Linkman，参数linkman中，如有有字段为空，代表不更新此字段，linkmanExample参数为where条件
	 *
	 * @param linkman
	 * @param linkmanExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateByLinkmanExampleSelective(Linkman linkman, LinkmanExample linkmanExample);

	/**
	 * 更新Linkman，参数linkman中，如有有字段为空，代表对应也更新为空，linkmanExample参数为where条件
	 *
	 * @param linkman
	 * @param linkmanExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateByLinkmanExample(Linkman linkman, LinkmanExample linkmanExample);

	/**
	 * 根据主键更新Linkman，参数linkman中，如果字段为空，代表不更新此字段
	 *
	 * @param linkman
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateLinkmanByPrimaryKeySelective(Linkman linkman);

	/**
	 * 根据主键更新Linkman，参数linkman中，如果字段为空，代表相应字段也更新为空
	 *
	 * @param linkman
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateLinkmanByPrimaryKey(Linkman linkman);

	/**
	 * 描述：根据用户ID，查找该用户下的所有联系人信息
	 *
	 * @param user
	 * @return List<Linkman>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Linkman> selectLinkmanByUser(User user);

}
