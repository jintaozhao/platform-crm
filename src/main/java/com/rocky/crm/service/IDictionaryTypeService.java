package com.rocky.crm.service;

import com.rocky.crm.pojo.DictionaryType;
import com.rocky.crm.pojo.DictionaryTypeExample;

import java.util.List;

/**
 * @author Rocky
 */

public interface IDictionaryTypeService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param dictionaryTypeExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.1
	 * @since 1.8
	 */
	long countByDictionaryTypeExample(DictionaryTypeExample dictionaryTypeExample);

	/**
	 * 描述：按照Example 删除 DictionaryType
	 *
	 * @param dictionaryTypeExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByDictionaryTypeExample(DictionaryTypeExample dictionaryTypeExample);

	/**
	 * 描述：按照主键id删除DictionaryTypeExample
	 *
	 * @param id 数据字典 id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条DictionaryType数据 如字段为空，则插入null
	 *
	 * @param dictionaryType
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertDictionaryType(DictionaryType dictionaryType);

	/**
	 * 描述：插入一条DictionaryType数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param dictionaryType
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertDictionaryTypeSelective(DictionaryType dictionaryType);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param dictionaryTypeExample 查询条件
	 * @return List<DictionaryType> 含DictionaryType的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<DictionaryType> selectByDictionaryTypeExample(DictionaryTypeExample dictionaryTypeExample);

	/**
	 * 描述：按照DictionaryType 的id 查找 DictionaryType
	 *
	 * @param id 要查询的id
	 * @return DictionaryType 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	DictionaryType selectDictionaryTypeByPrimaryKey(Integer id);

	/**
	 * 描述：更新DictionaryType
	 *
	 * @param dictionaryType        对象中若有空则更新字段为null
	 * @param dictionaryTypeExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByDictionaryTypeExample(DictionaryType dictionaryType, DictionaryTypeExample dictionaryTypeExample);

	/**
	 * 描述：更新DictionaryType
	 *
	 * @param dictionaryType        对象中若有空则不会更新此字段
	 * @param dictionaryTypeExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByDictionaryTypeExampleSelective(DictionaryType dictionaryType, DictionaryTypeExample dictionaryTypeExample);

	/**
	 * 描述：按照DictionaryType id 更新DictionaryType
	 *
	 * @param dictionaryType 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateDictionaryTypeByPrimaryKeySelective(DictionaryType dictionaryType);

	/**
	 * 描述：按照DictionaryType id 更新DictionaryType
	 *
	 * @param dictionaryType 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateDictionaryTypeByPrimaryKey(DictionaryType dictionaryType);
}
