package com.rocky.crm.service;

import com.rocky.crm.pojo.Role;
import com.rocky.crm.pojo.RoleExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IRoleService {

	/**
	 * 根据example统计role条数
	 *
	 * @param roleExample
	 * @return long
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	long countByRoleExample(RoleExample roleExample);

	/**
	 * 根据example删除role
	 *
	 * @param roleExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deleteByRoleExample(RoleExample roleExample);

	/**
	 * 根据主键删除role
	 *
	 * @param id
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean deleteRoleByPrimaryKey(Integer id);

	/**
	 * 插入一条role，字段为空，插入相应的字段也为空
	 *
	 * @param role
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean insertRole(Role role);

	/**
	 * 插入一条role，参数role中，如果字段为null，代表不插入此字段
	 *
	 * @param role
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean insertRoleSelective(Role role);

	/**
	 * 根据example查找role
	 *
	 * @param roleExample
	 * @return List<Role>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	List<Role> selectByRoleExample(RoleExample roleExample);

	/**
	 * 根据主键id查找role
	 *
	 * @param id
	 * @return Role
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	Role selectRoleByPrimaryKey(Integer id);

	/**
	 * 更新role，参数role中，如果某些字段为空，代表不更新此字段，example代表where条件
	 *
	 * @param role
	 * @param roleExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateByRoleExampleSelective(Role role, RoleExample roleExample);

	/**
	 * 更新role，参数role中，如果某些字段为空，代表相应的字段也更新为空，example代表where条件
	 *
	 * @param role
	 * @param roleExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateByRoleExample(Role role, RoleExample roleExample);

	/**
	 * 更新role，传入的参数中，如果某些字段为空，代表不更新此字段
	 *
	 * @param role
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateRoleByPrimaryKeySelective(Role role);

	/**
	 * 更新role，传入的参数中，如果某些字段为空，代表相应字段也更新为空
	 *
	 * @param role
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	boolean updateRoleByPrimaryKey(Role role);

}
