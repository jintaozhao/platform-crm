package com.rocky.crm.service;

import com.rocky.crm.pojo.Role;
import com.rocky.crm.pojo.SysDept;
import com.rocky.crm.pojo.SysDeptTree;
import com.rocky.crm.pojo.TreeNode;

import java.util.List;
import java.util.Map;

/**
 * 部门管理 服务层
 *
 * @author Rocky
 */
public interface ISysDeptService {
	/**
	 * 查询部门管理数据
	 *
	 * @param dept 部门信息
	 * @return 部门信息集合
	 */
	List<SysDept> selectDeptList(SysDept dept);

	/**
	 * 查询该部门下所有子部门
	 *
	 * @param deptId 部门id
	 * @return
	 */
	List<SysDeptTree> selectChildren(Long deptId);

	/**
	 * 查询部门管理树
	 *
	 * @param dept 部门信息
	 * @return 所有部门信息
	 */
	List<TreeNode> selectDeptTree(SysDept dept);

	/**
	 * 根据角色ID查询菜单
	 *
	 * @param role 角色对象
	 * @return 菜单列表
	 */
	List<Map<String, Object>> roleDeptTreeData(Role role);

	/**
	 * 查询部门人数
	 *
	 * @param parentId 父部门ID
	 * @return 结果
	 */
	int selectDeptCount(Long parentId);

	/**
	 * 查询部门是否存在用户
	 *
	 * @param deptId 部门ID
	 * @return 结果 true 存在 false 不存在
	 */
	boolean checkDeptExistUser(Long deptId);

	/**
	 * 删除部门管理信息
	 *
	 * @param deptId 部门ID
	 * @return 结果
	 */
	int deleteDeptById(Long deptId);

	/**
	 * 删除部门管理信息 真删
	 *
	 * @param deptId 部门id
	 * @return
	 */
	int deleteDeptRealById(Long deptId);

	/**
	 * 新增保存部门信息
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	int insertDept(SysDept dept);

	/**
	 * 修改保存部门信息
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	int updateDept(SysDept dept);

	/**
	 * 根据部门ID查询信息
	 *
	 * @param deptId 部门ID
	 * @return 部门信息
	 */
	SysDept selectDeptById(Long deptId);

	/**
	 * @param deptName 部门名称
	 * @return
	 */
	SysDept selectDeptByName(String deptName);

	/**
	 * 根据用户id查询所在部门信息
	 *
	 * @param userId 用户id
	 * @return
	 */
	List<SysDept> selectDeptsByUserId(Long userId);

	/**
	 * 校验部门名称是否唯一
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	String checkDeptNameUnique(SysDept dept);
}
