package com.rocky.crm.service;

import com.rocky.crm.pojo.Category;
import com.rocky.crm.pojo.CategoryExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ICategoryService {
	/**
	 * 描述：按照Example 统计记录总数
	 *
	 * @param categoryExample category模板
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByCategoryExample(CategoryExample categoryExample);


	/**
	 * 描述：按照Example 删除category
	 *
	 * @param categoryExample
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByCategoryExample(CategoryExample categoryExample);

	/**
	 * 描述：
	 *
	 * @param id
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条category数据 如字段为空，则插入null
	 *
	 * @param category
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertCategory(Category category);

	/**
	 * 描述：插入一条category数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param category
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(Category category);

	/**
	 * 描述：按照Example 条件 模糊查询
	 *
	 * @param categoryExample
	 * @return List<Category>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Category> selectByCategoryExample(CategoryExample categoryExample);

	/**
	 * 描述：按照category 的id 查找 category
	 *
	 * @param id
	 * @return Category
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	Category selectCategoryByPrimaryKey(Integer id);

	/**
	 * 描述：更新category ， category对象中若有空则不会更新此字段  categoryExample为where条件
	 *
	 * @param category
	 * @param categoryExample
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCategoryExampleSelective(Category category, CategoryExample categoryExample);

	/**
	 * 描述：更新category， category对象中若有空则更新字段为null   categoryExample为where条件
	 *
	 * @param category
	 * @param categoryExample
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCategoryExample(Category category, CategoryExample categoryExample);

	/**
	 * 描述：按照category id 更新category  category对象中如有空则不会更新此字段
	 *
	 * @param category
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCategoryByPrimaryKeySelective(Category category);

	/**
	 * 描述：按照category id 更新category  category对象中如有空则更新此字段为null
	 *
	 * @param category
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCategoryByPrimaryKey(Category category);
}
