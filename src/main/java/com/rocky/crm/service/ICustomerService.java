package com.rocky.crm.service;

import com.rocky.crm.pojo.Customer;
import com.rocky.crm.pojo.CustomerExample;
import com.rocky.crm.pojo.Linkman;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/11
 */
public interface ICustomerService {

	/**
	 * 描述：按照Example 统计记录总数
	 *
	 * @param customerExample
	 * @return long
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	long countByCustomerExample(CustomerExample customerExample);

	/**
	 * 描述：按照Example 删除Customer
	 *
	 * @param customerExample
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByCustomerExample(CustomerExample customerExample);

	/**
	 * 描述：按照Customer主键id删除Customer
	 *
	 * @param id
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条Customer数据 如字段为空，则插入null
	 *
	 * @param customer
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertCustomer(Customer customer);

	/**
	 * 描述：插入一条Customer数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param customer
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(Customer customer);

	/**
	 * 描述：按照Example 条件 模糊查询
	 *
	 * @param customerExample
	 * @return List<Customer>
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	List<Customer> selectByCustomerExample(CustomerExample customerExample);

	/**
	 * 描述：按照Customer 的id 查找 Customer
	 *
	 * @param id
	 * @return Customer
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	Customer selectCustomerByPrimaryKey(Integer id);

	/**
	 * 描述：更新Customer ， Customer对象中若有空则不会更新此字段  CustomerExample为where条件
	 *
	 * @param customer
	 * @param customerExample
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCustomerExampleSelective(Customer customer, CustomerExample customerExample);

	/**
	 * 描述：更新Customer， Customer对象中若有空则更新字段为null   CustomerExample为where条件
	 *
	 * @param customer
	 * @param customerExample
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCustomerExample(Customer customer, CustomerExample customerExample);

	/**
	 * 描述：按照Customer id 更新Customer  Customer对象中如有空则不会更新此字段
	 *
	 * @param customer
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCustomerByPrimaryKeySelective(Customer customer);

	/**
	 * 描述：按照Customer id 更新Customer  Customer对象中如有空则更新此字段为null
	 *
	 * @param customer
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCustomerByPrimaryKey(Customer customer);

	/**
	 * 描述：添加客户，同时设置主要联系人
	 *
	 * @param customer
	 * @param linkman
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/11
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(Customer customer, Linkman linkman);
}
