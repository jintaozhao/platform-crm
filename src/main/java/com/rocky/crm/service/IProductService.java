package com.rocky.crm.service;

import com.rocky.crm.pojo.Product;
import com.rocky.crm.pojo.ProductExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IProductService {

	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param productExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByProductExample(ProductExample productExample);

	/**
	 * 描述：按照Example 删除Product
	 *
	 * @param productExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByProductExample(ProductExample productExample);

	/**
	 * 描述：按照Product主键id删除Product
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条Product数据 如字段为空，则插入null
	 *
	 * @param product 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertProduct(Product product);

	/**
	 * 描述：插入一条Product数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param product 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(Product product);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param productExample 查询条件
	 * @return List<Product> 含Product的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<Product> selectByProductExample(ProductExample productExample);

	/**
	 * 描述：按照Product 的id 查找 Product
	 *
	 * @param id 要查询的id
	 * @return Product 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	Product selectProductByPrimaryKey(Integer id);

	/**
	 * 描述：更新Product
	 *
	 * @param product        对象中若有空则更新字段为null
	 * @param productExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByProductExample(Product product, ProductExample productExample);

	/**
	 * 描述：更新Product
	 *
	 * @param product        对象中若有空则不会更新此字段
	 * @param productExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByProductExampleSelective(Product product, ProductExample productExample);

	/**
	 * 描述：按照Product id 更新Product
	 *
	 * @param product 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateProductByPrimaryKeySelective(Product product);

	/**
	 * 描述：按照Product id 更新Product
	 *
	 * @param product 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateProductByPrimaryKey(Product product);
}
