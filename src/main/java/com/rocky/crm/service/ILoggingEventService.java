package com.rocky.crm.service;

import com.rocky.crm.pojo.LoggingEvent;
import com.rocky.crm.pojo.LoggingEventExample;
import com.rocky.crm.pojo.Pager;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ILoggingEventService {

	/**
	 * 描述：按照Example 统计记录总数
	 *
	 * @param loggingEventExample
	 * @return long
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	long countByLoggingEventExample(LoggingEventExample loggingEventExample);

	/**
	 * 描述：按照Example 删除LoggingEvent
	 *
	 * @param loggingEventExample
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByLoggingEventExample(LoggingEventExample loggingEventExample);

	/**
	 * 描述：按照LoggingEvent主键id删除LoggingEvent
	 *
	 * @param id
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Long id);

	/**
	 * 描述：插入一条LoggingEvent数据 如字段为空，则插入null
	 *
	 * @param loggingEvent
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertLoggingEvent(LoggingEvent loggingEvent);

	/**
	 * 描述： 插入一条LoggingEvent数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param loggingEvent
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(LoggingEvent loggingEvent);

	/**
	 * 描述：按照Example 条件 模糊查询
	 *
	 * @param loggingEventExample
	 * @param pager
	 * @return List<LoggingEvent>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<LoggingEvent> selectByLoggingEventExample(LoggingEventExample loggingEventExample, Pager pager);

	/**
	 * 描述：按照LoggingEvent 的id 查找 LoggingEvent
	 *
	 * @param id
	 * @return LoggingEvent
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	LoggingEvent selectLoggingEventByPrimaryKey(Long id);

	/**
	 * 描述：更新LoggingEvent ， LoggingEvent对象中若有空则不会更新此字段  LoggingEventExample为where条件
	 *
	 * @param loggingEvent
	 * @param loggingEventExample
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByLoggingEventExampleSelective(LoggingEvent loggingEvent, LoggingEventExample loggingEventExample);

	/**
	 * 描述：更新LoggingEvent， LoggingEvent对象中若有空则更新字段为null   LoggingEventExample为where条件
	 *
	 * @param loggingEvent
	 * @param loggingEventExample
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByLoggingEventExample(LoggingEvent loggingEvent, LoggingEventExample loggingEventExample);

	/**
	 * 描述：按照LoggingEvent id 更新LoggingEvent  LoggingEvent对象中如有空则不会更新此字段
	 *
	 * @param loggingEvent
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateLoggingEventByPrimaryKeySelective(LoggingEvent loggingEvent);

	/**
	 * 描述：按照LoggingEvent id 更新LoggingEvent  LoggingEvent对象中如有空则更新此字段为null
	 *
	 * @param loggingEvent
	 * @return boolean
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateLoggingEventByPrimaryKey(LoggingEvent loggingEvent);

	boolean insertLog(LoggingEvent loggingEvent);
}
