package com.rocky.crm.service.impl;

import com.rocky.crm.mapper.DictionaryItemMapper;
import com.rocky.crm.mapper.DictionaryTypeMapper;
import com.rocky.crm.pojo.DictionaryItemExample;
import com.rocky.crm.pojo.DictionaryType;
import com.rocky.crm.pojo.DictionaryTypeExample;
import com.rocky.crm.service.IDictionaryTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author Rocky
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class DictionaryTypeServiceImpl implements IDictionaryTypeService {

	@Autowired
	private DictionaryTypeMapper dictionaryTypeMapper;

	@Autowired
	private DictionaryItemMapper dictionaryItemMapper;

	@Override
	public long countByDictionaryTypeExample(DictionaryTypeExample dictionaryTypeExample) {
		return dictionaryTypeMapper.countByExample(dictionaryTypeExample);
	}

	@Override
	public boolean deleteByDictionaryTypeExample(DictionaryTypeExample dictionaryTypeExample) {
		return dictionaryTypeMapper.deleteByExample(dictionaryTypeExample) > 0;
	}

	@Override
	public boolean deleteByPrimaryKey(Integer id) {
		return dictionaryTypeMapper.deleteByPrimaryKey(id) > 0;
	}

	@Override
	public boolean insertDictionaryType(DictionaryType dictionaryType) {
		int res = 0;
		try {
			res = dictionaryTypeMapper.insert(dictionaryType);
		} catch (Exception e) {
			System.out.println("添加DictionaryType 时出现一个异常.");
			e.printStackTrace();
		}
		return res > 0;
	}

	@Override
	public boolean insertDictionaryTypeSelective(DictionaryType dictionaryType) {
		int res = 0;
		try {
			res = dictionaryTypeMapper.insertSelective(dictionaryType);
		} catch (Exception e) {
			System.out.println("添加DictionaryType 时出现一个异常.");
			e.printStackTrace();
		}
		return res > 0;
	}

	@Override
	public List<DictionaryType> selectByDictionaryTypeExample(DictionaryTypeExample dictionaryTypeExample) {

		List<DictionaryType> list = dictionaryTypeMapper.selectByExample(dictionaryTypeExample);

		for (DictionaryType dictionaryType : list) {
			DictionaryItemExample example = new DictionaryItemExample();
			example.createCriteria().andTypeIdEqualTo(dictionaryType.getId());
			dictionaryType.setDictionaryItems(dictionaryItemMapper.selectByExample(example));
		}

		return list;
	}

	@Override
	public DictionaryType selectDictionaryTypeByPrimaryKey(Integer id) {
		DictionaryType dictionaryType = dictionaryTypeMapper.selectByPrimaryKey(id);
		DictionaryItemExample example = new DictionaryItemExample();
		example.createCriteria().andTypeIdEqualTo(dictionaryType.getId());
		dictionaryType.setDictionaryItems(dictionaryItemMapper.selectByExample(example));
		return dictionaryType;
	}

	@Override
	public boolean updateByDictionaryTypeExample(DictionaryType dictionaryType,
	                                             DictionaryTypeExample dictionaryTypeExample) {
		return dictionaryTypeMapper.updateByExample(dictionaryType, dictionaryTypeExample) > 0;
	}

	@Override
	public boolean updateByDictionaryTypeExampleSelective(DictionaryType dictionaryType,
	                                                      DictionaryTypeExample dictionaryTypeExample) {
		return dictionaryTypeMapper.updateByExampleSelective(dictionaryType, dictionaryTypeExample) > 0;
	}

	@Override
	public boolean updateDictionaryTypeByPrimaryKeySelective(DictionaryType dictionaryType) {
		return dictionaryTypeMapper.updateByPrimaryKeySelective(dictionaryType) > 0;
	}

	@Override
	public boolean updateDictionaryTypeByPrimaryKey(DictionaryType dictionaryType) {
		return dictionaryTypeMapper.updateByPrimaryKey(dictionaryType) > 0;
	}

}
