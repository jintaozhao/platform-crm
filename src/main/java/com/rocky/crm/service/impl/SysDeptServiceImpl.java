package com.rocky.crm.service.impl;

import com.rocky.crm.constants.UserConstants;
import com.rocky.crm.exception.BusinessException;
import com.rocky.crm.mapper.SysDeptMapper;
import com.rocky.crm.pojo.Role;
import com.rocky.crm.pojo.SysDept;
import com.rocky.crm.pojo.SysDeptTree;
import com.rocky.crm.pojo.TreeNode;
import com.rocky.crm.service.ISysDeptService;
import com.rocky.crm.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 部门管理 服务实现
 *
 * @author Rocky
 */
@Service
public class SysDeptServiceImpl implements ISysDeptService {
	@Autowired
	private SysDeptMapper deptMapper;

	/**
	 * 查询部门管理数据
	 *
	 * @param dept 部门信息
	 * @return 部门信息集合
	 */
	@Override
	public List<SysDept> selectDeptList(SysDept dept) {
		return deptMapper.selectDeptList(dept);
	}

	/**
	 * 查询该部门下所有子部门
	 *
	 * @param deptId 部门id
	 * @return
	 */
	@Override
	public List<SysDeptTree> selectChildren(Long deptId) {
		return deptMapper.selectChildren(deptId);
	}

	/**
	 * 查询部门管理树
	 *
	 * @param dept 部门信息
	 * @return 所有部门信息
	 */
	@Override
	public List<TreeNode> selectDeptTree(SysDept dept) {
		List<TreeNode> trees = new ArrayList<>();

		List<SysDept> deptList = deptMapper.selectDeptList(dept);
		for (SysDept sysDept : deptList) {
			TreeNode treeNode = new TreeNode();
			treeNode.setId(sysDept.getDeptId());
			treeNode.setName(sysDept.getDeptName());
			treeNode.setParentId(sysDept.getParentId());
			if (sysDept.getParentId() == 0) {
				treeNode.setSpread(true);
			}
			trees.add(treeNode);
		}
		trees = buildTree(trees, 0L);
		return trees;
	}

	private List<TreeNode> buildTree(List<TreeNode> list, Long rootDeptId) {
		List<TreeNode> tree = new ArrayList<>();
		for (TreeNode node : list) {
			if (node.getParentId().equals(rootDeptId)) {
				List<TreeNode> deptTreeList = buildTree(list, node.getId());
				node.setChildren(deptTreeList);
				tree.add(node);
			}
		}
		return tree;
	}

	/**
	 * 根据角色ID查询部门（数据权限）
	 *
	 * @param role 角色对象
	 * @return 部门列表（数据权限）
	 */
	@Override
	public List<Map<String, Object>> roleDeptTreeData(Role role) {
		Integer roleId = role.getId();
		List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
		List<SysDept> deptList = selectDeptList(new SysDept());
		if (StringUtils.isNotNull(roleId)) {
			List<String> roleDeptList = deptMapper.selectRoleDeptTree(roleId);
			trees = getTrees(deptList, true, roleDeptList);
		} else {
			trees = getTrees(deptList, false, null);
		}
		return trees;
	}

	/**
	 * 对象转部门树
	 *
	 * @param deptList     部门列表
	 * @param isCheck      是否需要选中
	 * @param roleDeptList 角色已存在菜单列表
	 * @return
	 */
	public List<Map<String, Object>> getTrees(List<SysDept> deptList, boolean isCheck, List<String> roleDeptList) {

		List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
		for (SysDept dept : deptList) {
			if (UserConstants.DEPT_NORMAL.equals(dept.getStatus())) {
				Map<String, Object> deptMap = new HashMap<String, Object>();
				deptMap.put("id", dept.getDeptId());
				deptMap.put("pId", dept.getParentId());
				deptMap.put("name", dept.getDeptName());
				deptMap.put("title", dept.getDeptName());
				if (isCheck) {
					deptMap.put("checked", roleDeptList.contains(dept.getDeptId() + dept.getDeptName()));
				} else {
					deptMap.put("checked", false);
				}
				trees.add(deptMap);
			}
		}
		return trees;
	}

	/**
	 * 查询部门人数
	 *
	 * @param parentId 部门ID
	 * @return 结果
	 */
	@Override
	public int selectDeptCount(Long parentId) {
		SysDept dept = new SysDept();
		dept.setParentId(parentId);
		return deptMapper.selectDeptCount(dept);
	}

	/**
	 * 查询部门是否存在用户
	 *
	 * @param deptId 部门ID
	 * @return 结果 true 存在 false 不存在
	 */
	@Override
	public boolean checkDeptExistUser(Long deptId) {
		int result = deptMapper.checkDeptExistUser(deptId);
		return result > 0;
	}

	/**
	 * 删除部门管理信息
	 *
	 * @param deptId 部门ID
	 * @return 结果
	 */
	@Override
	public int deleteDeptById(Long deptId) {
		return deptMapper.deleteDeptById(deptId);
	}

	/**
	 * 删除部门管理信息 真删
	 *
	 * @param deptId 部门id
	 * @return
	 */
	@Override
	public int deleteDeptRealById(Long deptId) {
		return deptMapper.deleteDeptRealById(deptId);
	}

	/**
	 * 新增保存部门信息
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public int insertDept(SysDept dept) {
		SysDept info = deptMapper.selectDeptById(dept.getParentId());
		// 如果父节点不为"正常"状态,则不允许新增子节点
		if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
			throw new BusinessException("部门停用，不允许新增");
		}
		dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
		return deptMapper.insertDept(dept);
	}

	/**
	 * 修改保存部门信息
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public int updateDept(SysDept dept) {
		SysDept info = deptMapper.selectDeptById(dept.getParentId());
		if (StringUtils.isNotNull(info)) {
			String ancestors = info.getAncestors() + "," + info.getDeptId();
			dept.setAncestors(ancestors);
			updateDeptChildren(dept.getDeptId(), ancestors);
		}
		int result = deptMapper.updateDept(dept);
		if (UserConstants.DEPT_NORMAL.equals(dept.getStatus())) {
			// 如果该部门是启用状态，则启用该部门的所有上级部门
			updateParentDeptStatus(dept);
		}
		return result;
	}

	/**
	 * 修改该部门的父级部门状态
	 *
	 * @param dept 当前部门
	 */
	private void updateParentDeptStatus(SysDept dept) {
		String updateUser = dept.getUpdateUser();
		dept = deptMapper.selectDeptById(dept.getDeptId());
		dept.setUpdateUser(updateUser);
		deptMapper.updateDeptStatus(dept);
	}

	/**
	 * 修改子元素关系
	 *
	 * @param deptId    部门ID
	 * @param ancestors 元素列表
	 */
	public void updateDeptChildren(Long deptId, String ancestors) {
		SysDept dept = new SysDept();
		dept.setParentId(deptId);
		List<SysDept> childrens = deptMapper.selectDeptList(dept);
		for (SysDept children : childrens) {
			children.setAncestors(ancestors + "," + dept.getParentId());
		}
		if (childrens.size() > 0) {
			deptMapper.updateDeptChildren(childrens);
		}
	}

	/**
	 * 根据部门ID查询信息
	 *
	 * @param deptId 部门ID
	 * @return 部门信息
	 */
	@Override
	public SysDept selectDeptById(Long deptId) {
		return deptMapper.selectDeptById(deptId);
	}

	/**
	 * @param deptName 部门名称
	 * @return
	 */
	@Override
	public SysDept selectDeptByName(String deptName) {
		SysDept dept = new SysDept();
		dept.setDeptName(deptName);
		List<SysDept> deptList = deptMapper.selectDeptList(dept);
		return deptList.size() > 0 ? deptList.get(0) : null;
	}

	/**
	 * 根据用户id查询所在部门信息
	 *
	 * @param userId 用户id
	 * @return
	 */
	@Override
	public List<SysDept> selectDeptsByUserId(Long userId) {
		List<SysDept> userDepts = deptMapper.selectDeptsByUserId(userId);
		List<SysDept> depts = deptMapper.selectDeptAll();
		for (SysDept dept : depts) {
			for (SysDept userDept : userDepts) {
				if (dept.getDeptId().longValue() == userDept.getDeptId().longValue()) {
					dept.setFlag(true);
					break;
				}
			}
		}
		return depts;
	}

	/**
	 * 校验部门名称是否唯一
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public String checkDeptNameUnique(SysDept dept) {
		Long deptId = StringUtils.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
		SysDept info = deptMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
		if (StringUtils.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue()) {
			return UserConstants.DEPT_NAME_NOT_UNIQUE;
		}
		return UserConstants.DEPT_NAME_UNIQUE;
	}
}
