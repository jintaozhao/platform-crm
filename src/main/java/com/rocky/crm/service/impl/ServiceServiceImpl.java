package com.rocky.crm.service.impl;

import com.rocky.crm.mapper.CustomerMapper;
import com.rocky.crm.mapper.ServiceMapper;
import com.rocky.crm.mapper.UserMapper;
import com.rocky.crm.pojo.ServiceExample;
import com.rocky.crm.service.IServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ServiceServiceImpl implements IServiceService {

	@Autowired
	private ServiceMapper serviceMapper;

	@Autowired
	private CustomerMapper customerMapper;

	@Autowired
	private UserMapper userMapper;

	@Override
	public long countByServiceExample(ServiceExample serviceExample) {
		return serviceMapper.countByExample(serviceExample);
	}

	@Override
	public boolean deleteByServiceExample(ServiceExample serviceExample) {
		return serviceMapper.deleteByExample(serviceExample) > 0;
	}

	@Override
	public boolean deleteByPrimaryKey(Integer id) {
		return serviceMapper.deleteByPrimaryKey(id) > 0;
	}

	@Override
	public boolean insertService(com.rocky.crm.pojo.Service service) {
		service.setDeleteStatus(0);
		return serviceMapper.insert(service) > 0;
	}

	@Override
	public boolean insertSelective(com.rocky.crm.pojo.Service service) {
		return serviceMapper.insertSelective(service) > 0;
	}

	@Override
	public List<com.rocky.crm.pojo.Service> selectByServiceExample(ServiceExample serviceExample) {

		List<com.rocky.crm.pojo.Service> services = serviceMapper.selectByExample(serviceExample);
		//进行数据的二次封装
		for (com.rocky.crm.pojo.Service service : services) {
			if (service.getCreater() != null) {
				service.setCreaterObject(userMapper.selectByPrimaryKey(service.getCreater()));
			}
			if (service.getHandler() != null) {
				service.setHandlerObject(userMapper.selectByPrimaryKey(service.getHandler()));
			}
			if (service.getCustomerId() != null) {
				service.setCustomer(customerMapper.selectByPrimaryKey(service.getCustomerId()));
			}
		}
		return services;
	}

	@Override
	public com.rocky.crm.pojo.Service selectServiceByPrimaryKey(Integer id) {
		com.rocky.crm.pojo.Service service = serviceMapper.selectByPrimaryKey(id);
		if (service.getCreater() != null) {
			service.setCreaterObject(userMapper.selectByPrimaryKey(service.getCreater()));
		}
		if (service.getHandler() != null) {
			service.setHandlerObject(userMapper.selectByPrimaryKey(service.getHandler()));
		}
		if (service.getCustomerId() != null) {
			service.setCustomer(customerMapper.selectByPrimaryKey(service.getCustomerId()));
		}
		return service;
	}

	@Override
	public boolean updateByServiceExample(com.rocky.crm.pojo.Service service, ServiceExample serviceExample) {
		return serviceMapper.updateByExample(service, serviceExample) > 0;
	}

	@Override
	public boolean updateByServiceExampleSelective(com.rocky.crm.pojo.Service service, ServiceExample serviceExample) {
		return serviceMapper.updateByExampleSelective(service, serviceExample) > 0;
	}

	@Override
	public boolean updateServiceByPrimaryKey(com.rocky.crm.pojo.Service service) {
		return serviceMapper.updateByPrimaryKey(service) > 0;
	}

	@Override
	public boolean updateServiceByPrimaryKeySelective(com.rocky.crm.pojo.Service service) {
		return serviceMapper.updateByPrimaryKeySelective(service) > 0;
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see com.rocky.crm.service.IServiceService#deleteServicesByPrimaryKey(java.lang.String)
	 */
	@Override
	public boolean deleteServicesByPrimaryKey(String ids) {
		boolean success;
		try {
			//先把ids字符串根据规则分割开
			String[] idsStrings = ids.split("-");
			for (String idString : idsStrings) {
				Integer id = Integer.parseInt(idString);
				com.rocky.crm.pojo.Service service = new com.rocky.crm.pojo.Service();
				service.setId(id);
				service.setDeleteStatus(1);
				//如果删除失败，则直接抛出异常
				if (serviceMapper.updateByPrimaryKeySelective(service) < 1) {
					throw new RuntimeException();
				}
			}
			success = true;
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return success;
	}

}
