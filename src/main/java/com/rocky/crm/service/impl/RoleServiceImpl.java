package com.rocky.crm.service.impl;

import com.rocky.crm.mapper.RoleMapper;
import com.rocky.crm.pojo.Role;
import com.rocky.crm.pojo.RoleExample;
import com.rocky.crm.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Rocky
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RoleServiceImpl implements IRoleService {

	@Autowired
	private RoleMapper roleMapper;

	/**
	 * 根据example统计role条数
	 *
	 * @param roleExample
	 * @return long
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public long countByRoleExample(RoleExample roleExample) {
		return roleMapper.countByExample(roleExample);
	}

	/**
	 * 根据example删除role
	 *
	 * @param oleExample
	 * @return int
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean deleteByRoleExample(RoleExample roleExample) {
		int ret = roleMapper.deleteByExample(roleExample);
		return ret != 0;
	}

	/**
	 * 根据主键删除role
	 *
	 * @param id
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean deleteRoleByPrimaryKey(Integer id) {
		int ret = roleMapper.deleteByPrimaryKey(id);
		return ret != 0;
	}

	/**
	 * 插入一条role，字段为空，插入相应的字段也为空
	 *
	 * @param role
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean insertRole(Role role) {
		int ret = roleMapper.insert(role);
		return ret != 0;
	}

	/**
	 * 插入一条role，参数role中，如果字段为null，代表不插入此字段
	 *
	 * @param role
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean insertRoleSelective(Role role) {
		int ret = roleMapper.insertSelective(role);
		return ret != 0;
	}

	/**
	 * 根据example查找role
	 *
	 * @param roleExample
	 * @return List<Role>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public List<Role> selectByRoleExample(RoleExample roleExample) {
		return roleMapper.selectByExample(roleExample);
	}

	/**
	 * 根据主键id查找role
	 *
	 * @param id
	 * @return Role
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public Role selectRoleByPrimaryKey(Integer id) {
		return roleMapper.selectByPrimaryKey(id);
	}

	/**
	 * 更新role，参数role中，如果某些字段为空，代表不更新此字段，example代表where条件
	 *
	 * @param record
	 * @param example
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateByRoleExampleSelective(Role role, RoleExample roleExample) {
		int ret = roleMapper.updateByExampleSelective(role, roleExample);
		return ret != 0;
	}

	/**
	 * 更新role，参数role中，如果某些字段为空，代表相应的字段也更新为空，example代表where条件
	 *
	 * @param role
	 * @param roleExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateByRoleExample(Role role, RoleExample roleExample) {
		int ret = roleMapper.updateByExample(role, roleExample);
		return ret != 0;
	}

	/**
	 * 更新role，传入的参数中，如果某些字段为空，代表不更新此字段
	 *
	 * @param role
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateRoleByPrimaryKeySelective(Role role) {
		int ret = roleMapper.updateByPrimaryKeySelective(role);
		return ret != 0;
	}

	/**
	 * 更新role，传入的参数中，如果某些字段为空，代表相应字段也更新为空
	 *
	 * @param role
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateRoleByPrimaryKey(Role role) {
		int ret = roleMapper.updateByPrimaryKey(role);
		return ret != 0;
	}

}
