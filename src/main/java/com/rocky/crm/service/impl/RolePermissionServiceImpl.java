package com.rocky.crm.service.impl;

import com.rocky.crm.mapper.RolePermissionMapper;
import com.rocky.crm.pojo.RolePermission;
import com.rocky.crm.pojo.RolePermissionExample;
import com.rocky.crm.service.IRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Rocky
 */
@Service
@Transactional
public class RolePermissionServiceImpl implements IRolePermissionService {

	@Autowired
	private RolePermissionMapper rolePermissionMapper;

	/**
	 * 根据RolePermissionExample统计条数
	 *
	 * @param rolePermissionExample
	 * @return long
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public long countByRolePermissionExample(RolePermissionExample rolePermissionExample) {
		return rolePermissionMapper.countByExample(rolePermissionExample);
	}

	/**
	 * 根据RolePermissionExample删除rolePermission
	 *
	 * @param rolePermissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean deleteByRolePermissionExample(RolePermissionExample rolePermissionExample) {
		int ret = rolePermissionMapper.deleteByExample(rolePermissionExample);
		return ret != 0;
	}

	/**
	 * 根据主键id删除rolePermission
	 *
	 * @param id
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean deleteRolePermissionByPrimaryKey(Integer id) {
		int ret = rolePermissionMapper.deleteByPrimaryKey(id);
		return ret != 0;
	}

	/**
	 * 插入一条数据，如果字段为空，插入的字段也为空
	 *
	 * @param rolePermission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean insertRolePermission(RolePermission rolePermission) {
		int ret = rolePermissionMapper.insert(rolePermission);
		return ret != 0;
	}

	/**
	 * 插入一条数据，如果参数rolePermission中字段为空，代表字段为数据库默认值
	 *
	 * @param rolePermission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean insertRolePermissionSelective(RolePermission rolePermission) {
		int ret = rolePermissionMapper.insertSelective(rolePermission);
		return ret != 0;
	}

	/**
	 * 根据RolePermissionExample查找
	 *
	 * @param rolePermissionExample
	 * @return List<RolePermission>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public List<RolePermission> selectByRolePermissionExample(RolePermissionExample rolePermissionExample) {
		return rolePermissionMapper.selectByExample(rolePermissionExample);
	}

	/**
	 * 根据主键id查找RolePermission
	 *
	 * @param id
	 * @return RolePermission
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public RolePermission selectRolePermissionByPrimaryKey(Integer id) {
		return rolePermissionMapper.selectByPrimaryKey(id);
	}

	/**
	 * 更新RolePermission，如果传入的参数rolePermission中字段为空，代表不更新此字段，RolePermissionExample为where条件
	 *
	 * @param rolePermission
	 * @param rolePermissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateByRolePermissionExampleSelective(RolePermission rolePermission, RolePermissionExample rolePermissionExample) {
		int ret = rolePermissionMapper.updateByExampleSelective(rolePermission, rolePermissionExample);
		return ret != 0;
	}

	/**
	 * 更新RolePermission，如果传入的参数rolePermission中字段为空，代表此字段也更新为空，RolePermissionExample为where条件
	 *
	 * @param rolePermission
	 * @param rolePermissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateByRolePermissionExample(RolePermission rolePermission, RolePermissionExample rolePermissionExample) {
		int ret = rolePermissionMapper.updateByExample(rolePermission, rolePermissionExample);
		return ret != 0;
	}

	/**
	 * 更新RolePermission，如果传入的参数rolePermission中字段为空，代表不更新此字段
	 *
	 * @param rolePermission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateRolePermissionByPrimaryKeySelective(RolePermission rolePermission) {
		int ret = rolePermissionMapper.updateByPrimaryKeySelective(rolePermission);
		return ret != 0;
	}

	/**
	 * 更新RolePermission,如果传入的参数rolePermission中字段为空，那么相应字段也更新为空
	 *
	 * @param rolePermission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateRolePermissionByPrimaryKey(RolePermission rolePermission) {
		int ret = rolePermissionMapper.updateByPrimaryKey(rolePermission);
		return ret != 0;
	}

	/**
	 * 角色权限分配
	 *
	 * @param rolePermission
	 * @return int  插入的记录行数
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public int allotPermission(Integer[] permissionIds, Integer roleId) {
		int count = 0;
		try {
			//1. 先将角色原来的权限全部删除
			RolePermissionExample example = new RolePermissionExample();
			example.createCriteria().andRoleIdEqualTo(roleId);
			rolePermissionMapper.deleteByExample(example);

			//2. 重新插入该角色的权限
			if (permissionIds.length > 0) {
				count = rolePermissionMapper.insertRolePermission(permissionIds, roleId);
			}

		} catch (Exception e) {
			return 0;
		}

		return count;
	}

}
