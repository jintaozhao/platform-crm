package com.rocky.crm.service.impl;

import com.rocky.crm.mapper.DictionaryItemMapper;
import com.rocky.crm.pojo.DictionaryItem;
import com.rocky.crm.pojo.DictionaryItemExample;
import com.rocky.crm.service.IDictionaryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Rocky
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DictionaryItemServiceImpl implements IDictionaryItemService {

	@Autowired
	private DictionaryItemMapper dictionaryItemMapper;

	@Override
	public long countByDictionaryItemExample(DictionaryItemExample dictionaryItemExample) {
		return dictionaryItemMapper.countByExample(dictionaryItemExample);
	}

	@Override
	public boolean deleteByDictionaryItemExample(DictionaryItemExample dictionaryItemExample) {
		int res = 0;
		res = dictionaryItemMapper.deleteByExample(dictionaryItemExample);

		return res > 0;
	}

	@Override
	public boolean deleteByPrimaryKey(Integer id) {
		int res = 0;
		res = dictionaryItemMapper.deleteByPrimaryKey(id);
		return res > 0;
	}

	@Override
	public boolean insertDictionaryItem(DictionaryItem dictionaryItem) {
		int res = 0;
		try {
			//如果typeId不存在，尝试从item中的DictionaryType对象提取id
			if (dictionaryItem.getTypeId() == null) {
				dictionaryItem.setTypeId(dictionaryItem.getDictionaryType().getId());
			}
			res = dictionaryItemMapper.insert(dictionaryItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res > 0;

	}

	@Override
	public boolean insertDictionaryItemSelective(DictionaryItem dictionaryItem) {
		int res = 0;
		try {
			//如果typeId不存在，尝试从item中的DictionaryType对象提取id
			if (dictionaryItem.getTypeId() == null) {
				dictionaryItem.setTypeId(dictionaryItem.getDictionaryType().getId());
			}
			res = dictionaryItemMapper.insertSelective(dictionaryItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res > 0;
	}

	@Override
	public List<DictionaryItem> selectByDictionaryItemExample(DictionaryItemExample dictionaryItemExample) {
		return dictionaryItemMapper.selectByExample(dictionaryItemExample);
	}

	@Override
	public DictionaryItem selectDictionaryItemByPrimaryKey(Integer id) {
		return dictionaryItemMapper.selectByPrimaryKey(id);
	}

	@Override
	public boolean updateByDictionaryItemExample(DictionaryItem dictionaryItem,
	                                             DictionaryItemExample dictionaryItemExample) {
		int res = 0;
		res = dictionaryItemMapper.updateByExample(dictionaryItem, dictionaryItemExample);
		return res > 0;
	}

	@Override
	public boolean updateByDictionaryItemExampleSelective(DictionaryItem dictionaryItem,
	                                                      DictionaryItemExample dictionaryItemExample) {
		int res = 0;
		res = dictionaryItemMapper.updateByExampleSelective(dictionaryItem, dictionaryItemExample);
		return res > 0;
	}

	@Override
	public boolean updateDictionaryItemByPrimaryKeySelective(DictionaryItem dictionaryItem) {
		int res = 0;
		res = dictionaryItemMapper.updateByPrimaryKeySelective(dictionaryItem);
		return res > 0;
	}

	@Override
	public boolean updateDictionaryItemByPrimaryKey(DictionaryItem dictionaryItem) {
		int res = 0;
		res = dictionaryItemMapper.updateByPrimaryKey(dictionaryItem);
		return res > 0;
	}

}
