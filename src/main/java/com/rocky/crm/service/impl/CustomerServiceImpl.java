package com.rocky.crm.service.impl;

import com.rocky.crm.mapper.CustomerMapper;
import com.rocky.crm.mapper.LinkmanMapper;
import com.rocky.crm.mapper.ProductMapper;
import com.rocky.crm.mapper.UserMapper;
import com.rocky.crm.pojo.*;
import com.rocky.crm.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private CustomerMapper customerMapper;

	@Autowired
	private LinkmanMapper linkmanMapper;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private ProductMapper productMapper;


	@Override
	public long countByCustomerExample(CustomerExample customerExample) {
		return customerMapper.countByExample(customerExample);
	}

	@Override
	public boolean deleteByCustomerExample(CustomerExample customerExample) {
		return customerMapper.deleteByExample(customerExample) > 0;
	}

	@Override
	public boolean deleteByPrimaryKey(Integer id) {

		Customer customer = new Customer();
		customer.setId(id);
		customer.setDeleteStatus(1);

		return customerMapper.updateByPrimaryKeySelective(customer) > 0;
	}

	@Override
	public boolean insertCustomer(Customer customer) {
		return customerMapper.insert(customer) > 0;
	}

	@Override
	public boolean insertSelective(Customer customer) {
		return customerMapper.insertSelective(customer) > 0;

	}

	@Override
	public boolean insertSelective(Customer customer, Linkman linkman) {
		//插入客户数据
		if (customerMapper.insertSelective(customer) > 0) {

			CustomerExample example = new CustomerExample();
			example.createCriteria().andNameEqualTo(customer.getName());

			customer = customerMapper.selectByExample(example).get(0);

			linkman.setCustomerId(customer.getId());
			linkman.setLevel(0);

			linkmanMapper.insertSelective(linkman);

			return true;
		} else {
			return false;
		}

	}

	@Override
	public List<Customer> selectByCustomerExample(CustomerExample customerExample) {

		List<Customer> list = customerMapper.selectByExample(customerExample);

		for (Customer customer : list) {
			try {
				Product product = productMapper.selectByPrimaryKey(customer.getProductId());
				customer.setProduct(product);

				User creater = userMapper.selectByPrimaryKey(customer.getCreater());
				if (creater != null) {
					creater.setPassword(null);
					creater.setSalt(null);
				}
				customer.setCreaterObject(creater);

				User manager = userMapper.selectByPrimaryKey(customer.getManagerId());
				if (manager != null) {
					manager.setPassword(null);
					manager.setSalt(null);
				}
				customer.setManager(manager);
			} catch (Exception e) {
			}
		}
		return list;
	}

	@Override
	public Customer selectCustomerByPrimaryKey(Integer id) {
		Customer customer = customerMapper.selectByPrimaryKey(id);
		try {
			Product product = productMapper.selectByPrimaryKey(customer.getProductId());
			customer.setProduct(product);

			User creater = userMapper.selectByPrimaryKey(customer.getCreater());
			if (creater != null) {
				creater.setPassword(null);
				creater.setSalt(null);
			}
			customer.setCreaterObject(creater);

			User manager = userMapper.selectByPrimaryKey(customer.getManagerId());
			if (manager != null) {
				manager.setPassword(null);
				manager.setSalt(null);
			}
			customer.setManager(manager);
		} catch (Exception e) {
		}
		return customer;
	}

	@Override
	public boolean updateByCustomerExampleSelective(Customer customer, CustomerExample customerExample) {
		return customerMapper.updateByExampleSelective(customer, customerExample) > 0;
	}

	@Override
	public boolean updateByCustomerExample(Customer customer, CustomerExample customerExample) {
		return customerMapper.updateByExample(customer, customerExample) > 0;
	}

	@Override
	public boolean updateCustomerByPrimaryKeySelective(Customer customer) {
		return customerMapper.updateByPrimaryKeySelective(customer) > 0;
	}

	@Override
	public boolean updateCustomerByPrimaryKey(Customer customer) {
		return customerMapper.updateByPrimaryKey(customer) > 0;
	}

}
