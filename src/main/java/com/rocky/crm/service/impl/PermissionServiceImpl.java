package com.rocky.crm.service.impl;

import com.rocky.crm.mapper.PermissionMapper;
import com.rocky.crm.pojo.Pager;
import com.rocky.crm.pojo.Permission;
import com.rocky.crm.pojo.PermissionExample;
import com.rocky.crm.service.IPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Rocky
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PermissionServiceImpl implements IPermissionService {

	@Autowired
	private PermissionMapper permissionMapper;

	/**
	 * 根据example统计permission条数
	 *
	 * @param permissionExample+
	 * @return long
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public long countByPermissionExample(PermissionExample permissionExample) {
		return permissionMapper.countByExample(permissionExample);
	}

	/**
	 * 根据example删除permission
	 *
	 * @param permissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean deleteByPermissionExample(PermissionExample permissionExample) {
		int ret = permissionMapper.deleteByExample(permissionExample);
		return ret != 0;
	}

	/**
	 * 根据主键删除permission
	 *
	 * @param id
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean deletePermissionByPrimaryKey(Integer id) {
		int ret = permissionMapper.deleteByPrimaryKey(id);
		return ret != 0;
	}

	/**
	 * 插入一条permission，如果字段为空，那么插入null
	 *
	 * @param permission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean insertPermission(Permission permission) {
		int ret = permissionMapper.insert(permission);
		return ret != 0;
	}

	/**
	 * 插入一条permission，如果字段为空，插入的是数据库中字段的默认值
	 *
	 * @param permission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean insertPermissionSelective(Permission permission) {
		int ret = permissionMapper.insertSelective(permission);
		return ret != 0;
	}

	/**
	 * 根据example查找permission
	 *
	 * @param permissionExample
	 * @return List<Permission>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public List<Permission> selectByPermissionExample(PermissionExample permissionExample, Pager pager) {
		permissionExample.setLimit(pager.getPageSize());
		permissionExample.setOffset(new Long(pager.getOffset()));
		return permissionMapper.selectByExample(permissionExample);
	}

	/**
	 * 根据主键id查找permission
	 *
	 * @param id
	 * @return Permission
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public Permission selectPermissionByPrimaryKey(Integer id) {
		return permissionMapper.selectByPrimaryKey(id);
	}

	/**
	 * 更新permission，参数permission中如果某个字段为空代表不更新此字段，example为where条件
	 *
	 * @param permission
	 * @param permissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateByPermissionExampleSelective(Permission permission, PermissionExample permissionExample) {
		int ret = permissionMapper.updateByExampleSelective(permission, permissionExample);
		return ret != 0;
	}

	/**
	 * 更新permission，参数permission中如果某个字段为空代表此字段也更新为空，example为where条件
	 *
	 * @param permission
	 * @param permissionExample
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updateByPermissionExample(Permission permission, PermissionExample permissionExample) {
		int ret = permissionMapper.updateByExample(permission, permissionExample);
		return ret != 0;
	}

	/**
	 * 根据主键id更新permission，参数permission中某些字段为空，代表不更新此字段
	 *
	 * @param permission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updatePermissionByPrimaryKeySelective(Permission permission) {
		int ret = permissionMapper.updateByPrimaryKeySelective(permission);
		return ret != 0;
	}

	/**
	 * 根据主键id更新permission，参数permission中如果有字段为空，代表更新为null
	 *
	 * @param permission
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 */
	@Override
	public boolean updatePermissionByPrimaryKey(Permission permission) {
		int ret = permissionMapper.updateByPrimaryKey(permission);
		return ret != 0;
	}

	@Override
	public List<Permission> selectTreePermission() {
		return permissionMapper.selectTreePermission();
	}

	@Override
	public List<Permission> selectChildPermission(Integer id) {
		return permissionMapper.selectChildPermission(id);
	}

	@Override
	public boolean updatePermissionSetTopPermission(Permission permission) {

		return permissionMapper.setTopPermission(permission) > 0;
	}

	@Override
	public List<Permission> selectByPermissionExample(PermissionExample permissionExample) {

		return permissionMapper.selectByExample(permissionExample);
	}

}
