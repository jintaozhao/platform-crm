package com.rocky.crm.service.impl;

import com.rocky.crm.mapper.SysUserDeptMapper;
import com.rocky.crm.pojo.SysUserDept;
import com.rocky.crm.service.SysUserDeptService;
import com.rocky.crm.utils.ConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Rocky
 * @version 1.0.0
 * @classname SysUserDeptServiceImpl
 * @description <br>用户与部门关联服务层实现类</br>
 * @date 2019-09-27 16:05:08
 * @email jintaozhao@qq.com
 */
@Service
public class SysUserDeptServiceImpl implements SysUserDeptService {

	@Autowired
	private SysUserDeptMapper sysUserDeptMapper;

	/**
	 * 查询用户与部门关联信息
	 *
	 * @param userId 用户与部门关联ID
	 * @return 用户与部门关联信息
	 */
	@Override
	public List<SysUserDept> selectSysUserDeptById(Long userId) {
		return sysUserDeptMapper.selectSysUserDeptById(userId);
	}

	/**
	 * 查询用户与部门关联列表
	 *
	 * @param sysUserDept 用户与部门关联信息
	 * @return 用户与部门关联集合
	 */
	@Override
	public List<SysUserDept> selectSysUserDeptList(SysUserDept sysUserDept) {
		return sysUserDeptMapper.selectSysUserDeptList(sysUserDept);
	}

	/**
	 * 新增用户与部门关联
	 *
	 * @param sysUserDept 用户与部门关联信息
	 * @return 结果
	 */
	@Override
	public int insertSysUserDept(SysUserDept sysUserDept) {
		return sysUserDeptMapper.insertSysUserDept(sysUserDept);
	}

	/**
	 * 修改用户与部门关联
	 *
	 * @param sysUserDept 用户与部门关联信息
	 * @return 结果
	 */
	@Override
	public int updateSysUserDept(SysUserDept sysUserDept) {
		return sysUserDeptMapper.updateSysUserDept(sysUserDept);
	}

	/**
	 * 删除用户与部门关联
	 *
	 * @param userId 用户与部门关联ID
	 * @return 结果
	 */
	@Override
	public int deleteSysUserDeptById(Long userId) {
		return sysUserDeptMapper.deleteSysUserDeptById(userId);
	}

	/**
	 * 删除用户与部门关联对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteSysUserDeptByIds(String ids) {
		return sysUserDeptMapper.deleteSysUserDeptByIds(ConvertUtils.toStrArray(ids));
	}

}
