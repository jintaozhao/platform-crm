package com.rocky.crm.service;

import com.rocky.crm.pojo.ServiceTransfer;
import com.rocky.crm.pojo.ServiceTransferExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface IServiceTransferService {
	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param serviceTransferExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByServiceTransferExample(ServiceTransferExample serviceTransferExample);

	/**
	 * 描述：按照Example 删除ServiceTransfer
	 *
	 * @param serviceTransferExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByServiceTransferExample(ServiceTransferExample serviceTransferExample);

	/**
	 * 描述：按照ServiceTransfer主键id删除ServiceTransfer
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条ServiceTransfer数据 如字段为空，则插入null
	 *
	 * @param serviceTransfer 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertServiceTransfer(ServiceTransfer serviceTransfer);

	/**
	 * 描述：插入一条ServiceTransfer数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param serviceTransfer 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(ServiceTransfer serviceTransfer);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param serviceTransferExample 查询条件
	 * @return List<ServiceTransfer> 含ServiceTransfer的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<ServiceTransfer> selectByServiceTransferExample(ServiceTransferExample serviceTransferExample);

	/**
	 * 描述：按照ServiceTransfer 的id 查找 ServiceTransfer
	 *
	 * @param id 要查询的id
	 * @return ServiceTransfer 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	ServiceTransfer selectServiceTransferByPrimaryKey(Integer id);

	/**
	 * 描述：更新ServiceTransfer
	 *
	 * @param serviceTransfer        对象中若有空则更新字段为null
	 * @param serviceTransferExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByServiceTransferExample(ServiceTransfer serviceTransfer, ServiceTransferExample serviceTransferExample);

	/**
	 * 描述：更新ServiceTransfer
	 *
	 * @param serviceTransfer        对象中若有空则不会更新此字段
	 * @param serviceTransferExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByServiceTransferExampleSelective(ServiceTransfer serviceTransfer, ServiceTransferExample serviceTransferExample);

	/**
	 * 描述：按照ServiceTransfer id 更新ServiceTransfer
	 *
	 * @param serviceTransfer 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateServiceTransferByPrimaryKeySelective(ServiceTransfer serviceTransfer);

	/**
	 * 描述：按照ServiceTransfer id 更新ServiceTransfer
	 *
	 * @param serviceTransfer 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateServiceTransferByPrimaryKey(ServiceTransfer serviceTransfer);
}
