package com.rocky.crm.service;

import com.rocky.crm.pojo.CpuInfoVo;
import com.rocky.crm.pojo.ServerInfo;

import java.util.List;
import java.util.Map;

public interface IServerInfoService {
	/**
	 * 描述：获取系统的基本信息
	 *
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	ServerInfo getServerInfo();

	/**
	 * 描述：获取cpu信息
	 *
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<CpuInfoVo> getCpuInfo();

	/**
	 * 描述：获取内存信息
	 *
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	Map<String, Object> getMemoryInfo();
}
