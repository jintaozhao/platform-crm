package com.rocky.crm.service;

import com.rocky.crm.pojo.CustomerCare;
import com.rocky.crm.pojo.CustomerCareExample;

import java.util.List;

/**
 * @author Rocky
 * @date 2017/07/24
 */
public interface ICustomerCareService {

	/**
	 * 描述： 按照Example 统计记录总数
	 *
	 * @param customerCareExample 查询条件
	 * @return long 数据的数量
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	long countByCustomerCareExample(CustomerCareExample customerCareExample);

	/**
	 * 描述：按照Example 删除CustomerCare
	 *
	 * @param customerCareExample
	 * @return boolean 删除的结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByCustomerCareExample(CustomerCareExample customerCareExample);

	/**
	 * 描述：按照CustomerCare主键id删除CustomerCare
	 *
	 * @param id 数据字典id
	 * @return boolean 删除结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean deleteByPrimaryKey(Integer id);

	/**
	 * 描述：插入一条CustomerCare数据 如字段为空，则插入null
	 *
	 * @param customerCare 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertCustomerCare(CustomerCare customerCare);

	/**
	 * 描述：插入一条CustomerCare数据，如字段为空，则插入数据库表字段的默认值
	 *
	 * @param customerCare 客户数据
	 * @return boolean 插入结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean insertSelective(CustomerCare customerCare);

	/**
	 * 描述：按照Example条件 模糊查询
	 *
	 * @param customerCareExample 查询条件
	 * @return List<CustomerCare> 含CustomerCare的list
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<CustomerCare> selectByCustomerCareExample(CustomerCareExample customerCareExample);

	/**
	 * 描述：按照CustomerCare 的id 查找 CustomerCare
	 *
	 * @param id 要查询的id
	 * @return CustomerCare 查到的数据或空值
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	CustomerCare selectCustomerCareByPrimaryKey(Integer id);

	/**
	 * 描述：更新CustomerCare
	 *
	 * @param customerCare        对象中若有空则更新字段为null
	 * @param customerCareExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCustomerCareExample(CustomerCare customerCare, CustomerCareExample customerCareExample);

	/**
	 * 描述：更新CustomerCare
	 *
	 * @param customerCare        对象中若有空则不会更新此字段
	 * @param customerCareExample 为where条件
	 * @return boolean 更新结果
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateByCustomerCareExampleSelective(CustomerCare customerCare, CustomerCareExample customerCareExample);

	/**
	 * 描述：按照CustomerCare id 更新CustomerCare
	 *
	 * @param customerCare 对象中如有空则不会更新此字段
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCustomerCareByPrimaryKeySelective(CustomerCare customerCare);

	/**
	 * 描述：按照CustomerCare id 更新CustomerCare
	 *
	 * @param customerCare 对象中如有空则更新此字段为null
	 * @return boolean
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	boolean updateCustomerCareByPrimaryKey(CustomerCare customerCare);

	/**
	 * 描述：根据客户经理ID查找
	 *
	 * @param id
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<CustomerCare> selectCustomerCareByManagerId(Integer id);
}
