package com.rocky.crm.mapper;

import com.rocky.crm.pojo.Role;
import com.rocky.crm.pojo.RoleExample;

/**
 * RoleMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface RoleMapper extends MyBatisBaseDao<Role, Integer, RoleExample> {
}