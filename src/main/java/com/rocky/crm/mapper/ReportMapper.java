package com.rocky.crm.mapper;

import com.rocky.crm.pojo.Report;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * 报表模块
 *
 * @author admin
 */
public interface ReportMapper {

	/**
	 * 描述：统计公司新增的客户量 近n天的记录
	 *
	 * @param n 天数
	 * @param
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<Map<String, Object>> countCustomerIncrease(@Param("n") int n);


	/**
	 * 描述：统计公司损失的客户量 近n天的记录
	 *
	 * @param n 天数
	 * @param
	 * @return
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<Map<String, Object>> countCustomerDecrease(@Param("n") int n);

	/**
	 * 描述：查询某段时间内客户的新增客户
	 *
	 * @param report
	 * @return List<Map   <   String   ,   Object>>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Map<String, Object>> countCustomerByDate(Report report);

	/**
	 * 描述：查询某段时间内客户的新增跟进记录
	 *
	 * @param report
	 * @return List<Map   <   String   ,   Object>>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Map<String, Object>> countFollowupByDate(Report report);

	/**
	 * 描述：查询某段时间内客户的新增客户,按照某个客户信息某个字段分类
	 *
	 * @param report
	 * @return List<Map   <   String   ,   Object>>
	 * @author Rocky
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Map<String, Object>> countCustomerByCategory(Report report);

	/**
	 * @param @param  report
	 * @param @return 参数
	 * @return List<Map   <   String   ,   Object>>    返回类型
	 * @throws
	 * @Title: countManagerCustomer
	 * @Description: TODO(统计某时段内客户经理客户量的排名)
	 * @author Rocky
	 */
	List<Map<String, Object>> countManagerCustomerRank(Report report);

	/**
	 * @param @param  report
	 * @param @return 参数
	 * @return List<Map   <   String   ,   Object>>    返回类型
	 * @throws
	 * @Title: countCustomerOrderPrice
	 * @Description: TODO(统计某时段内客户与公司成交的金额数排名)
	 * @author Rocky
	 */
	List<Map<String, Object>> customerOrderPriceRank(Report report);

	/**
	 * 描述：统计某个时段内某个客户经理服务的情况
	 *
	 * @param report
	 * @return List<Map   <   String   ,   Object>>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<Map<String, Object>> countManagerSerivce(Report report);

	/**
	 * 描述：统计公司的客户来源情况
	 *
	 * @param report
	 * @return List<Map   <   String   ,   Object>>
	 * @throws
	 * @author Rocky
	 * @version 1.0
	 * @since 1.8
	 */
	List<Map<String, Object>> countCustomerSource();
}
