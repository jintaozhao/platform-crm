package com.rocky.crm.mapper;

import com.rocky.crm.pojo.FollowUp;
import com.rocky.crm.pojo.FollowUpExample;

/**
 * FollowUpMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface FollowUpMapper extends MyBatisBaseDao<FollowUp, Integer, FollowUpExample> {
}