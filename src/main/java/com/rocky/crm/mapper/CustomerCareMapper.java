package com.rocky.crm.mapper;

import com.rocky.crm.pojo.CustomerCare;
import com.rocky.crm.pojo.CustomerCareExample;

/**
 * CustomerCareMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface CustomerCareMapper extends MyBatisBaseDao<CustomerCare, Integer, CustomerCareExample> {
}