package com.rocky.crm.mapper;

import com.rocky.crm.pojo.Linkman;
import com.rocky.crm.pojo.LinkmanExample;

/**
 * LinkmanMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface LinkmanMapper extends MyBatisBaseDao<Linkman, Integer, LinkmanExample> {
}