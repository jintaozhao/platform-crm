package com.rocky.crm.mapper;

import com.rocky.crm.pojo.Log;
import com.rocky.crm.pojo.LogExample;

/**
 * LogMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface LogMapper extends MyBatisBaseDao<Log, Integer, LogExample> {
}