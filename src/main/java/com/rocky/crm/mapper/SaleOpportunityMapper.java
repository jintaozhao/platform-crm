package com.rocky.crm.mapper;

import com.rocky.crm.pojo.SaleOpportunity;
import com.rocky.crm.pojo.SaleOpportunityExample;

/**
 * SaleOpportunityMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface SaleOpportunityMapper extends MyBatisBaseDao<SaleOpportunity, Integer, SaleOpportunityExample> {
}