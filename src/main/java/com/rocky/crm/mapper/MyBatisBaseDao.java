package com.rocky.crm.mapper;

import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * DAO公共基类，由MybatisGenerator自动生成请勿修改
 *
 * @param <Model> The Model Class
 * @param <PK>    The Primary Key Class
 * @param <E>     The Example Class
 * @author MybatisGenerator
 */
public interface MyBatisBaseDao<Model, PK extends Serializable, E> {
	/**
	 * 描述：根据example统计符合条件的数据量
	 *
	 * @param example
	 * @return long
	 * @author MybatisGenerator
	 * @version 1.0
	 * @since 1.8
	 */
	long countByExample(E example);

	/**
	 * 描述：根据example删除数据
	 *
	 * @param example
	 * @return int
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int deleteByExample(E example);

	/**
	 * 描述：根据主键删除数据
	 *
	 * @param id
	 * @return int
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int deleteByPrimaryKey(PK id);

	/**
	 * 描述：插入数据（若数据为空则设对应的字段为空)
	 *
	 * @param record
	 * @return int
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int insert(Model record);

	/**
	 * 描述：插入数据，若属性不存在则对应的字段设为数据库默认值
	 *
	 * @param record
	 * @return int
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int insertSelective(Model record);

	/**
	 * 描述：查询数据
	 *
	 * @param example
	 * @return List<Model>
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	List<Model> selectByExample(E example);

	/**
	 * 描述：根据主键查询数据
	 *
	 * @param id
	 * @return Model
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	Model selectByPrimaryKey(PK id);

	/**
	 * 描述：更新数据,若属性为空则对应字段不更新
	 *
	 * @param record
	 * @param example
	 * @return int
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int updateByExampleSelective(@Param("record") Model record, @Param("example") E example);

	/**
	 * 描述：更新数据，若属性为空则对应字段也设置为空
	 *
	 * @param record
	 * @param example
	 * @return int
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int updateByExample(@Param("record") Model record, @Param("example") E example);

	/**
	 * 描述：根据主键更新数据,若属性为空则对应字段不更新
	 *
	 * @param record
	 * @return int
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int updateByPrimaryKeySelective(Model record);

	/**
	 * 描述：根据主键更新数据，若属性为空则对应字段也设置为空
	 *
	 * @param record
	 * @return int
	 * @author MybatisGenerator
	 * @date 2017/07/24
	 * @version 1.0
	 * @since 1.8
	 */
	int updateByPrimaryKey(Model record);
}