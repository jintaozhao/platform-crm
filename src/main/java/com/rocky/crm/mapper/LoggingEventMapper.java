package com.rocky.crm.mapper;


import com.rocky.crm.pojo.LoggingEvent;
import com.rocky.crm.pojo.LoggingEventExample;

import java.util.List;

/**
 * LoggingEventMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface LoggingEventMapper extends MyBatisBaseDao<LoggingEvent, Long, LoggingEventExample> {

	/**
	 * 描述：
	 *
	 * @param @param  loggingEventExample
	 * @param @return 参数
	 * @return List<LoggingEvent>    返回类型
	 * @throws
	 * @Title: selectByExampleWithBLOBs
	 * @Description: TODO(查询含有text文本类型的结果集)
	 * @author Rocky
	 */
	List<LoggingEvent> selectByExampleWithBLOBs(LoggingEventExample loggingEventExample);

	int insertLog(LoggingEvent loggingEvent);
}