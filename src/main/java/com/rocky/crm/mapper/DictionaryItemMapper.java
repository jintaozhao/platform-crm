package com.rocky.crm.mapper;

import com.rocky.crm.pojo.DictionaryItem;
import com.rocky.crm.pojo.DictionaryItemExample;

/**
 * DictionaryItemMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface DictionaryItemMapper extends MyBatisBaseDao<DictionaryItem, Integer, DictionaryItemExample> {
}