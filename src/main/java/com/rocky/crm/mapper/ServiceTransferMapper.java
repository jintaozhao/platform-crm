package com.rocky.crm.mapper;

import com.rocky.crm.pojo.ServiceTransfer;
import com.rocky.crm.pojo.ServiceTransferExample;

/**
 * ServiceTransferMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface ServiceTransferMapper extends MyBatisBaseDao<ServiceTransfer, Integer, ServiceTransferExample> {
}