package com.rocky.crm.mapper;

import com.rocky.crm.pojo.OrderItem;
import com.rocky.crm.pojo.OrderItemExample;

/**
 * OrderItemMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface OrderItemMapper extends MyBatisBaseDao<OrderItem, Integer, OrderItemExample> {
}