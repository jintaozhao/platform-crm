package com.rocky.crm.mapper;

import com.rocky.crm.pojo.Service;
import com.rocky.crm.pojo.ServiceExample;

/**
 * ServiceMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface ServiceMapper extends MyBatisBaseDao<Service, Integer, ServiceExample> {
}