package com.rocky.crm.mapper;

import com.rocky.crm.pojo.Product;
import com.rocky.crm.pojo.ProductExample;

/**
 * ProductMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface ProductMapper extends MyBatisBaseDao<Product, Integer, ProductExample> {

}