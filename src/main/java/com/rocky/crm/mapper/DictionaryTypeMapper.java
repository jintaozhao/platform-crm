package com.rocky.crm.mapper;

import com.rocky.crm.pojo.DictionaryType;
import com.rocky.crm.pojo.DictionaryTypeExample;

/**
 * DictionaryTypeMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface DictionaryTypeMapper extends MyBatisBaseDao<DictionaryType, Integer, DictionaryTypeExample> {
}