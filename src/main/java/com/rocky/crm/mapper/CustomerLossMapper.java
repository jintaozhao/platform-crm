package com.rocky.crm.mapper;

import com.rocky.crm.pojo.CustomerLoss;
import com.rocky.crm.pojo.CustomerLossExample;
import com.rocky.crm.pojo.Orders;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * CustomerLossMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface CustomerLossMapper extends MyBatisBaseDao<CustomerLoss, Integer, CustomerLossExample> {

	/**
	 * 描述：
	 *
	 * @param @param  orders
	 * @param @return 参数
	 * @return int    返回类型
	 * @throws
	 * @Title: insertLossBatch
	 * @Description: TODO(批量插入)
	 * @author Rocky
	 */
	int insertLossBatch(@Param("orders") List<Orders> orders);
}