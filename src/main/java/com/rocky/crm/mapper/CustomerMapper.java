package com.rocky.crm.mapper;

import com.rocky.crm.pojo.Customer;
import com.rocky.crm.pojo.CustomerExample;

/**
 * CustomerMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface CustomerMapper extends MyBatisBaseDao<Customer, Integer, CustomerExample> {
}