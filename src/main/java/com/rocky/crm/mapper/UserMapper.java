package com.rocky.crm.mapper;

import com.rocky.crm.pojo.User;
import com.rocky.crm.pojo.UserExample;

/**
 * UserMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface UserMapper extends MyBatisBaseDao<User, Integer, UserExample> {
}