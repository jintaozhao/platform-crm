package com.rocky.crm.mapper;

import com.rocky.crm.pojo.Category;
import com.rocky.crm.pojo.CategoryExample;

/**
 * CategoryMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface CategoryMapper extends MyBatisBaseDao<Category, Integer, CategoryExample> {
}