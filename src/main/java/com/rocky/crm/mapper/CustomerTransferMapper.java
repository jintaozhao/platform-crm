package com.rocky.crm.mapper;

import com.rocky.crm.pojo.CustomerTransfer;
import com.rocky.crm.pojo.CustomerTransferExample;

/**
 * CustomerTransferMapper继承基类
 *
 * @author MybatisGenerator
 */
public interface CustomerTransferMapper extends MyBatisBaseDao<CustomerTransfer, Integer, CustomerTransferExample> {
}