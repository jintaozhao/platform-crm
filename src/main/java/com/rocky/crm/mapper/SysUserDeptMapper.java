package com.rocky.crm.mapper;

import com.rocky.crm.pojo.SysUserDept;

import java.util.List;

/**
 * @author Rocky
 * @version 1.0.0
 * @classname SysUserDeptMapper
 * @description <br>用户与部门关联数据层</br>
 * @date 2019-09-27 16:05:08
 * @email jintaozhao@qq.com
 */
public interface SysUserDeptMapper {
	/**
	 * 查询用户与部门关联信息
	 *
	 * @param userId 用户ID
	 * @return 用户与部门关联信息
	 */
	List<SysUserDept> selectSysUserDeptById(Long userId);

	/**
	 * @param deptId 用户与部门关联ID
	 * @return
	 */
	List<SysUserDept> selectSysUserDeptByDeptId(Long deptId);

	/**
	 * 查询用户与部门关联列表
	 *
	 * @param sysUserDept 用户与部门关联信息
	 * @return 用户与部门关联集合
	 */
	List<SysUserDept> selectSysUserDeptList(SysUserDept sysUserDept);

	/**
	 * 新增用户与部门关联
	 *
	 * @param sysUserDept 用户与部门关联信息
	 * @return 结果
	 */
	int insertSysUserDept(SysUserDept sysUserDept);

	/**
	 * 修改用户与部门关联
	 *
	 * @param sysUserDept 用户与部门关联信息
	 * @return 结果
	 */
	int updateSysUserDept(SysUserDept sysUserDept);

	/**
	 * 删除用户与部门关联
	 *
	 * @param userId 用户与部门关联ID
	 * @return 结果
	 */
	int deleteSysUserDeptById(Long userId);

	/**
	 * 批量删除用户与部门关联
	 *
	 * @param userIds 需要删除的数据ID
	 * @return 结果
	 */
	int deleteSysUserDeptByIds(String[] userIds);

}