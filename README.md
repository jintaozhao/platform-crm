## 麦芒客户关系管理系统
 以极简方式管理客户和业务过程


- 系统开发环境
  - 操作系统： Windows 10
  - 集成开发工具： idea 2018
  - 编译环境：JDK 1.8
  - Web服务器：Tomcat 8.0.45
  - 数据库： MySQL 5.7
  - 缓存: Redis-server 3.2
  
- 系统部署环境
  
  - CentOS 7.0
  - Tomcat 8.0+
  - MySQL 5.7+ 8.0
  - Redis-server

- 系统框架
  - Spring框架
  - SpringMVC框架
  - MyBatis框架
  - Logback日志框架
  - 安全验证框架
  - maven框架
  - layUI前端框架
  - Shiro安全框架

- 系统核心技术
  - 基于角色的权限访问控制RBCA（Role-Based Access Control）
  - Spring+SpringMVC+MyBatis三大框架
  - Ajax技术
  - SpringMVC文件上传
  - Shiro安全框架
  - Redis 缓存
  - JavaMail邮件
  - SpringMVC 基于aop切面 的日志管理
  - Layui 前端框架
  - 登录验证码
  
- 系统功能介绍
  - 客户管理
    - 我的客户：管理我的客户
    - 销售机会：添加销售机会
    - 联系跟进：进行客户跟踪，以时间轴方式呈现
    - 客户流失：统计流失客户及挽救措施管理
    - 合同订单：客户签约管理记录
  
  - 产品管理
    
    - 产品分类：公司提供的产品及服务分类大项  
    - 产品列表：详细产品（服务）列表 
  - 客户服务
    
    - 我的服务：为自己客户提供的服务，以及服务处理等
    - 服务统计：销售自己的历史服务（处理）统计
    - 服务统计（全）:所有销售人员提供的服务统计 

  - 客户关怀
    
    - 统计7天内，15天内，30天内过生日客户，提前为客户准备关怀服务等。
    
  - 数据统计
   
    客户统计，分类统计，业绩统计，客户成交量统计等
    
  - 个人中心
  
    提供个人资料修改及密码修改等
    
  - 数据字典
  
    配置系统通用数据字典项
    
  - 系统管理
  
    用户管理，部门管理，角色管理，权限管理，日志管理，系统信息等模块
- 部分页面预览
  - 登录页
![登录页](https://images.gitee.com/uploads/images/2019/1226/104930_6de842fb_23477.png "login.png")
  - 首页
![首页](https://images.gitee.com/uploads/images/2019/1226/105029_11b2d785_23477.png "index.png")
  - 客户管理
![客户管理](https://images.gitee.com/uploads/images/2019/1226/105236_67d9702c_23477.png "customer.png")
  - 产品管理
![产品管理](https://images.gitee.com/uploads/images/2019/1226/105348_5983edc2_23477.png "product.png")
  - 客户服务
![客户服务](https://images.gitee.com/uploads/images/2019/1226/105444_39ae8f5f_23477.png "service.png")
  - 客户关怀
![客户关怀](https://images.gitee.com/uploads/images/2019/1226/105531_0e318b32_23477.png "care.png")
  - 数据统计
![数据 统计](https://images.gitee.com/uploads/images/2019/1226/105711_835487b4_23477.png "statistics.png")
  - 个人中心
![个人中心](https://images.gitee.com/uploads/images/2019/1226/105803_7c119050_23477.png "ucenter.png")
  - 数据字典
![数据字典](https://images.gitee.com/uploads/images/2019/1226/105854_45dc741c_23477.png "dictionary.png")
  - 系统管理